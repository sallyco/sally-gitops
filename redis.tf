resource "helm_release" "redis" {
  name       = "redis"
  namespace = kubernetes_namespace.production.metadata.0.name
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "redis"
  version    = "18.6.1"
  values     = [
    file("${path.root}/helm_values/redis.yaml")
  ]
}
