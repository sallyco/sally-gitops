resource "aws_s3_bucket" "onboarding_prod" {
  bucket = "sally-onboarding-prod"
  force_destroy = true

  tags = {
    Name        = "Sally Onboarding Static App"
    Environment = var.environment
  }
}

resource "aws_s3_bucket_versioning" "onboarding_prod_versioning" {
  bucket = aws_s3_bucket.onboarding_prod.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_public_access_block" "onboarding_prod_public_access_block" {
  bucket = aws_s3_bucket.onboarding_prod.id

  block_public_acls   = false
  block_public_policy = false
  ignore_public_acls  = false
  restrict_public_buckets = false
}


resource "aws_s3_bucket_policy" "onboarding_prod_policy" {
  bucket = aws_s3_bucket.onboarding_prod.id

  policy = jsonencode({
    Version = "2012-10-17",
    Id      = "PolicyForCloudFrontPrivateContent",
    Statement = [
      {
            "Sid": "PublicReadGetObject",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.onboarding_prod.bucket}/*"
      },
      {
        Sid       = "AllowCloudFrontServicePrincipal",
        Effect    = "Allow",
        Principal = {
          Service = "cloudfront.amazonaws.com"
        },
        Action   = "s3:GetObject",
        Resource = "arn:aws:s3:::${aws_s3_bucket.onboarding_prod.bucket}/*",
        Condition = {
          StringEquals = {
            "AWS:SourceArn" = aws_cloudfront_distribution.onboarding_distribution.arn
          }
        }
      }
    ]
  })
}

resource "aws_s3_bucket_website_configuration" "onboarding_prod_website" {
  bucket = aws_s3_bucket.onboarding_prod.id

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "index.html"
  }
}

resource "aws_cloudfront_distribution" "onboarding_distribution" {
  origin {
    domain_name = "${aws_s3_bucket.onboarding_prod.bucket}.s3-website-${var.region}.amazonaws.com"
    origin_id   = aws_s3_bucket.onboarding_prod.bucket

    custom_origin_config {
      http_port                = 80
      https_port               = 443
      origin_protocol_policy   = "http-only"
      origin_ssl_protocols     = ["TLSv1", "TLSv1.1", "TLSv1.2"]
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  default_root_object = "index.html"

  aliases = ["onboarding.${var.domain_name}"]

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = aws_s3_bucket.onboarding_prod.bucket

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  price_class = "PriceClass_100"

  viewer_certificate {
    acm_certificate_arn            = var.acm_certificate_arn
    ssl_support_method             = "sni-only"
    minimum_protocol_version       = "TLSv1.2_2021"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    Name = "sallyCloudFrontDistribution"
  }
}

resource "aws_cloudfront_origin_access_identity" "onboarding_oai" {
  comment = "OAI for Sally Onboarding App"
}

resource "aws_route53_record" "onboarding_dns" {
  zone_id = data.aws_route53_zone.this[0].zone_id
  name    = "onboarding.${var.domain_name}"
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.onboarding_distribution.domain_name
    zone_id                = aws_cloudfront_distribution.onboarding_distribution.hosted_zone_id
    evaluate_target_health = false
  }
}