resource "kubernetes_namespace" "production" {
  metadata {
    name = "production"
    labels = {
      name = "production"
    }
  }
}
resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = "monitoring"
  }
}
resource "kubernetes_namespace" "vault" {
  metadata {
    name = "vault"
  }
}
