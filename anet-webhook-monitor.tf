# Generate a random password for the application (optional)
resource "random_password" "anet_admin_password" {
  length = 25
}

# Create a Kubernetes Secret to store sensitive information
resource "kubernetes_secret" "anet_webhook_credentials" {
  metadata {
    name      = "anet-webhook-secret"
    namespace = kubernetes_namespace.production.metadata[0].name
  }
  type = "Opaque"
  data = {
    API_LOGIN_ID     = base64encode(var.anet_api_login_id)
    TRANSACTION_KEY  = base64encode(var.anet_transaction_key)
  }
}

# Kubernetes Deployment for anet-webhook-monitor
resource "kubernetes_deployment" "anet_webhook_monitor" {
  metadata {
    name      = "anet-webhook-monitor"
    namespace = kubernetes_namespace.production.metadata[0].name
    labels = {
      app = "anet-webhook-monitor"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "anet-webhook-monitor"
      }
    }

    template {
      metadata {
        labels = {
          app = "anet-webhook-monitor"
        }
      }

      spec {
        container {
          name  = "anet-webhook-monitor"
          image = "registry.gitlab.com/sallyco/devops/anet-webhook-monitor:latest"
          image_pull_policy = "Always"

          env {
            name = "API_LOGIN_ID"
            value_from {
              secret_key_ref {
                name = kubernetes_secret.anet_webhook_credentials.metadata[0].name
                key  = "API_LOGIN_ID"
              }
            }
          }

          env {
            name = "TRANSACTION_KEY"
            value_from {
              secret_key_ref {
                name = kubernetes_secret.anet_webhook_credentials.metadata[0].name
                key  = "TRANSACTION_KEY"
              }
            }
          }

          env {
            name  = "NODE_ENV"
            value = "production"
          }

          env {
            name  = "PORT"
            value = "9000"
          }

          port {
            container_port = 9000
            name           = "http"
          }

          readiness_probe {
            http_get {
              path = "/"
              port = "http"
            }
            initial_delay_seconds = 30
            period_seconds        = 10
          }

          liveness_probe {
            http_get {
              path = "/"
              port = "http"
            }
            initial_delay_seconds = 60
            period_seconds        = 20
          }

          resources {
            limits = {
              cpu    = "200m"
              memory = "256Mi"
            }
            requests = {
              cpu    = "100m"
              memory = "128Mi"
            }
          }
        }
      }
    }
  }
}

# Kubernetes Service for anet-webhook-monitor
resource "kubernetes_service" "anet_webhook_service" {
  metadata {
    name      = "anet-webhook-service"
    namespace = kubernetes_namespace.production.metadata[0].name
    labels = {
      app = "anet-webhook-monitor"
    }
  }

  spec {
    selector = {
      app = "anet-webhook-monitor"
    }

    port {
      name        = "http"
      port        = 80
      target_port = 9000
      protocol    = "TCP"
    }

    type = "ClusterIP"
  }
}

# Kubernetes Ingress for anet-webhook-monitor
resource "kubernetes_ingress_v1" "anet_webhook_ingress" {
  metadata {
    name      = "anet-webhook-ingress"
    namespace = kubernetes_namespace.production.metadata[0].name
    annotations = {
      "alb.ingress.kubernetes.io/scheme"           = "internet-facing"
      "alb.ingress.kubernetes.io/group.name"       = "internal-service"
      "alb.ingress.kubernetes.io/target-type"      = "ip"
      "cert-manager.io/cluster-issuer"             = "cert-manager-webhook-ca"
      "alb.ingress.kubernetes.io/listen-ports"     = "[{\"HTTP\":80}, {\"HTTPS\":443}]"
      "alb.ingress.kubernetes.io/ssl-redirect"     = "443"
    }
    labels = {
      app = "anet-webhook-monitor"
    }
  }

  spec {
    ingress_class_name = "alb"

    rule {
      host = "anet-webhook.sally.co"  # Replace with your actual hostname

      http {
        path {
          path      = "/*"
          path_type = "ImplementationSpecific"

          backend {
            service {
              name = kubernetes_service.anet_webhook_service.metadata[0].name
              port {
                number = 80
              }
            }
          }
        }
      }
    }

    tls {
      hosts       = ["anet-webhook.sally.co"]  # Replace with your actual hostname
      secret_name = "anet-webhook-tls"
    }
  }
}

# (Optional) Define the Kubernetes Namespace if not already defined
# resource "kubernetes_namespace" "production" {
#   metadata {
#     name = "production"
#   }
# }
