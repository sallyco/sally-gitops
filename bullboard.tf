resource "random_password" "bullboard_admin_password" {
  length = 25
}

resource "kubernetes_secret" "bullboard_credentials" {
  metadata {
    name      = "bullboard-secret"
    namespace = kubernetes_namespace.production.metadata.0.name
  }
  type = "Opaque"
  data = {
    BULLBOARD_USER     = base64encode("admin")
    BULLBOARD_PASSWORD = base64encode(random_password.bullboard_admin_password.result)
  }
}

resource "kubernetes_deployment" "bullboard" {
  metadata {
    name      = "bullboard"
    namespace = kubernetes_namespace.production.metadata[0].name
    labels = {
      app = "bullboard"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "bullboard"
      }
    }

    template {
      metadata {
        labels = {
          app = "bullboard"
        }
      }

      spec {
        container {
          name  = "bullboard"
          image = "registry.gitlab.com/sallyco/devops/bullboard:latest"
          image_pull_policy = "Always"

          env {
            name  = "REDIS_HOST"
            value = "redis-master.${kubernetes_namespace.production.metadata[0].name}.svc.cluster.local"
          }

          # Inject BULLBOARD_USER and BULLBOARD_PASSWORD from the secret
          env {
            name = "BULLBOARD_USER"
            value_from {
              secret_key_ref {
                name = kubernetes_secret.bullboard_credentials.metadata[0].name
                key  = "BULLBOARD_USER"
              }
            }
          }

          env {
            name = "BULLBOARD_PASSWORD"
            value_from {
              secret_key_ref {
                name = kubernetes_secret.bullboard_credentials.metadata[0].name
                key  = "BULLBOARD_PASSWORD"
              }
            }
          }

          port {
            container_port = 3000
            name           = "http"
          }

          readiness_probe {
            http_get {
              path = "/health"
              port = "http"
            }
            initial_delay_seconds = 30
            period_seconds        = 10
          }

          liveness_probe {
            http_get {
              path = "/health"
              port = "http"
            }
            initial_delay_seconds = 60
            period_seconds        = 20
          }

          resources {
            limits = {
              cpu    = "200m"
              memory = "256Mi"
            }
            requests = {
              cpu    = "100m"
              memory = "128Mi"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "bullboard" {
  metadata {
    name      = "bullboard-service"
    namespace = kubernetes_namespace.production.metadata[0].name
    labels = {
      app = "bullboard"
    }
  }

  spec {
    selector = {
      app = "bullboard"
    }

    port {
      name        = "http"
      port        = 80
      target_port = 3000
      protocol    = "TCP"
    }

    type = "ClusterIP"
  }
}

resource "kubernetes_ingress_v1" "bullboard" {
  metadata {
    name      = "bullboard-ingress"
    namespace = kubernetes_namespace.production.metadata[0].name
    annotations = {
      "alb.ingress.kubernetes.io/scheme"       = "internet-facing"
      "alb.ingress.kubernetes.io/group.name"   = "internal-service"
      "alb.ingress.kubernetes.io/target-type"  = "ip"
      "cert-manager.io/cluster-issuer"         = "cert-manager-webhook-ca"
      "alb.ingress.kubernetes.io/listen-ports" = "[{\"HTTP\":80}, {\"HTTPS\":443}]"
      "alb.ingress.kubernetes.io/ssl-redirect" = "443"
    }
    labels = {
      app = "bullboard"
    }
  }

  spec {
    ingress_class_name = "alb"

    rule {
      host = "bullboard.sally.co" # Replace with your actual hostname

      http {
        path {
          path      = "/*"
          path_type = "ImplementationSpecific"

          backend {
            service {
              name = kubernetes_service.bullboard.metadata[0].name
              port {
                number = 80
              }
            }
          }
        }
      }
    }

    tls {
      hosts       = ["bullboard.sally.co"] # Replace with your actual hostname
      secret_name = "bullboard-tls"
    }
  }
}
