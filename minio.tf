resource "aws_kms_key" "minio" {
  description = "Sally production MinIO Encryption/Decryption Key"
  tags = {
    Name = "Sally production MinIO Encryption/Decryption Key"
  }
}

resource "aws_kms_alias" "minio_alias" {
  name          = "alias/minio2/${var.name}"
  target_key_id = aws_kms_key.minio.key_id
}

resource "aws_iam_user" "s3_user" {
  name = "prod-docs-sally-s3"
}

resource "aws_iam_user_policy" "s3_user_policy" {
  name = "prod-docs-sally-s3"
  user = aws_iam_user.s3_user.name

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetBucketLocation",
                "s3:ListAllMyBuckets",
                "s3:GetObject",
                "s3:ListBucket",
                "s3:CreateBucket",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": "arn:aws:s3:::*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
                "s3:ListBucket",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": "arn:aws:s3:::*/*"
        },
        {
            "Effect": "Allow",
            "Action": [
              "kms:GenerateDataKey", 
              "kms:Decrypt"
            ],
            "Resource": "${aws_kms_key.minio.arn}"
        }
    ]
}
EOF
}
#
resource "aws_iam_access_key" "s3_user_access_key" {
  user = aws_iam_user.s3_user.name
}

resource "random_password" "minio_access_key" {
  length = 20
}
resource "random_password" "minio_secret_key" {
  length = 40
  special          = true
  override_special = "!#$%&()*+,-.:;<=>?[]^_{|}~"
}

resource "random_password" "minio_root_password" {
  length = 20
}
resource "kubernetes_secret" "minio_root_password" {
  metadata {
    name = "minio-root-password"
    namespace = kubernetes_namespace.production.metadata[0].name
  }

  data = {
    "root-password" = base64encode(random_password.minio_root_password.result)
  }
}

resource "helm_release" "minio" {
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "minio"
  version    = "12.0.1"
  name       = "minio-service"
  namespace  = kubernetes_namespace.production.metadata[0].name
  values     = [
    file("${path.root}/helm_values/minio.yaml")
  ]

  set {
    name  = "existingSecret"
    value = kubernetes_secret.minio_root_password.metadata[0].name
  }

  set_sensitive {
    name  = "accessKey"
    value = random_password.minio_access_key.result
  }

  set_sensitive {
    name  = "secretKey"
    value = random_password.minio_secret_key.result
  }

  set_sensitive {
    name  = "gateway.auth.s3.accessKey"
    value = aws_iam_access_key.s3_user_access_key.id
  }

  set_sensitive {
    name  = "gateway.auth.s3.secretKey"
    value = aws_iam_access_key.s3_user_access_key.secret
  }
}
