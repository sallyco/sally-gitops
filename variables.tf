# General
variable "name" {
  description = "Name that will be used for all resources as identifier"
  type        = string
  default     = "sally"
}
variable "environment" {
  description = "Sally environment"
  type        = string
  default     = "prod"
}
variable "vpc_cidr" {
  description = "VPC CIDR"
  type        = string
  default     = "10.0.0.0/16"
}
variable "region" {
  description = "AWS region"
  type        = string
  default     = "us-east-1"
}

variable "domain_name" {
  description = "Domain Name for Sally Inc."
  type        = string
  default     = "sally.co"
}

# EKS Cluster
variable "instance_type" {
  description = "EC2 instance type for the nodes in the EKS cluster"
  type        = string
  default     = "m5.xlarge"
}
variable "desired_capacity" {
  description = "Desided capacity for the scaling config for the node group"
  type        = string
  default     = 2
}
variable "max_size" {
  description = "Maximum size for the scaling config for the node group"
  type        = string
  default     = 5
}
variable "min_size" {
  description = "Minimum size for the scaling config for the node group"
  type        = string
  default     = 2
}
variable "node_group_name" {
  description = "Name of the node group"
  type        = string
  default     = "general"
}
variable "kubernetes_version" {
  description = "Kubernetes version"
  type        = string
  default     = "1.29"
}

# Default Addons
variable "addons" {
  description = "Kubernetes addons"
  type        = any
  default = {
    enable_aws_load_balancer_controller = true
    enable_external_dns                 = true
    enable_metrics_server               = true
    enable_cert_manager                 = true
    enable_karpenter                    = true
    enable_kube_prometheus_stack        = false
    enable_aws_argocd_ingress           = true
    enable_cluster_autoscaler           = true
    enable_prometheus_adapter           = false
  }
}

# Addons Git
variable "gitops_addons_org" {
  description = "Git repository org/user contains for addons"
  type        = string
  default     = "https://gitlab.com/sallyco"
}
variable "gitops_addons_repo" {
  description = "Git repository contains for addons"
  type        = string
  default     = "eks-blueprints-add-ons.git"
}
variable "gitops_addons_revision" {
  description = "Git repository revision/branch/ref for addons"
  type        = string
  default     = "main"
}
variable "gitops_addons_basepath" {
  description = "Git repository base path for addons"
  type        = string
  default     = "argocd/"
}
variable "gitops_addons_path" {
  description = "Git repository path for addons"
  type        = string
  default     = "bootstrap/control-plane/addons"
}

# Keycloak
variable "keycloak_username" {
  description = "Sally Keycloak Username"
  type        = string
  default     = "keycloak"
}

variable "keycloak_db_name" {
  description = "Sally Keycloak Database Name"
  type        = string
  default     = "keycloakprodsally"
}

# Camunda
# variable "camunda_db_name" {
#   description = "Sally Camunda Database Name"
#   type        = string
#   default     = "processengine"
#   
# }

# Workloads Git
variable "gitops_workload_org" {
  description = "Git repository org/user contains for workload"
  type        = string
  default     = "https://gitlab.com/sallyco"
}
variable "gitops_workload_repo" {
  description = "Git repository contains for workload"
  type        = string
  default     = "sally-gitops.git"
}
variable "gitops_workload_revision" {
  description = "Git repository revision/branch/ref for workload"
  type        = string
  default     = "main"
}
variable "gitops_workload_path" {
  description = "Git repository path for workload"
  type        = string
  default     = "k8s"
}

variable "enable_gitops_auto_addons" {
  description = "Automatically deploy addons"
  type        = bool
  default     = false
}

variable "enable_gitops_auto_workloads" {
  description = "Automatically deploy addons"
  type        = bool
  default     = false
}

variable "bastion_host_ami" {
  description = "AMI for bastion host"
  type        = string
  default     = "ami-079db87dc4c10ac91"
}

variable "bastion_host_instance_type" {
  description = "Instance type for bastion host"
  type        = string
  default     = "t2.micro"
}

variable "bastion_host_volume_size" {
  description = "Storage size for bastion host"
  type        = string
  default     = "8"
}

variable "bastion_host_volume_type" {
  description = "Storage type for bastion host"
  type        = string
  default     = "gp3"
}

variable "bastion_host_key_pair_name" {
  description = "Key name for bastion host"
  type        = string
  default     = "sally-general-purpose-key-pair"
}

variable "alternate_urls" {
  description = "List of alternate URLs for the Sally App CloudFront distribution"
  type        = list(string)
  default = [
    "app.sally.co",
    "spvs.sally.co",
    "test.sally.co",
    "ecphora.sally.co",
    "egis.sally.co",
    "software.spvconcierge.com",
    "revand.sally.co",
    "sv.sally.co",
    "capsule-capital.sally.co",
    "catalog.sally.co",
    "hourglass.sally.co",
    "invest.sallyventures.com",
    "portal.egislaw.com",
    "portal.revand.io",

  ]
}
variable "sandbox_alternate_urls" {
  description = "List of alternate URLs for the Sally App CloudFront distribution - Sandbox"
  type        = list(string)
  default = [
    "app-sandbox.sally.co"
  ]
}

variable "bastion_host_cidr_blocks" {
  description = "CIDR blocks for bastion host"
  type        = list(string)
  default = [
    "69.162.242.183/32", # Torby's IP
    "202.47.36.70/32",
    "202.47.36.70/32", #Afham's IP
    "119.73.96.142/32" # Zaman's IP
  ]
}

variable "acm_certificate_arn" {
  description = "ARN of the ACM certificate for CloudFront distribution"
  type        = string
  default     = "arn:aws:acm:us-east-1:360217847587:certificate/96af13a5-4362-4d02-b380-679746f46f1b"
}

variable "anet_api_login_id" {
  description = "Authorize.Net API Login ID"
  type        = string
  default     = "3knUVb7W2y"
}

variable "anet_transaction_key" {
  description = "Authorize.Net Transaction Key"
  type        = string
  default     = "2r89a5rfW45E5MD4"
}