resource "kubernetes_secret" "gitlab_registry" {
  metadata {
    name      = "gitlab-registry"
    namespace = kubernetes_namespace.production.metadata[0].name
  }
  type = "kubernetes.io/dockerconfigjson"
  binary_data = {
    ".dockerconfigjson" = "eyJhdXRocyI6IHsicmVnaXN0cnkuZ2l0bGFiLmNvbSI6IHsiYXV0aCI6ICJZV05qYjNWdWRITkFjMkZzYkhrdVkyODZaMnh3WVhRdGFHRjJlbWx1YTFWbmVtdGlObnA0UlhCNFdGVT0ifX19"
  }
}
