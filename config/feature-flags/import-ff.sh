#!/bin/bash

# Function to display usage
usage() {
    echo "Usage: $0 -t <access_token> -p <project_id> -f <json_file>"
    exit 1
}

# Parse command-line flags
while getopts ":t:p:f:" opt; do
  case $opt in
    t) ACCESS_TOKEN="$OPTARG"
    ;;
    p) PROJECT_ID="$OPTARG"
    ;;
    f) JSON_FILE="$OPTARG"
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
        usage
    ;;
  esac
done

# Check if all parameters are provided
if [ -z "$ACCESS_TOKEN" ] || [ -z "$PROJECT_ID" ] || [ -z "$JSON_FILE" ]; then
    echo "Missing required arguments"
    usage
fi

# Loop through each feature flag in the JSON file
jq -c '.[]' "$JSON_FILE" | while read -r flag; do
    curl --request POST --header "PRIVATE-TOKEN: $ACCESS_TOKEN" --header "Content-Type: application/json" --data "$flag" "https://gitlab.com/api/v4/projects/$PROJECT_ID/feature_flags"
done

