#!/bin/bash

# Function to display usage
usage() {
    echo "Usage: $0 -t <access_token> -p <project_id> -f <output_file>"
    exit 1
}

# Parse command-line flags
while getopts ":t:p:f:" opt; do
  case $opt in
    t) ACCESS_TOKEN="$OPTARG"
    ;;
    p) PROJECT_ID="$OPTARG"
    ;;
    f) OUTPUT_FILE="$OPTARG"
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
        usage
    ;;
  esac
done

# Check if all parameters are provided
if [ -z "$ACCESS_TOKEN" ] || [ -z "$PROJECT_ID" ] || [ -z "$OUTPUT_FILE" ]; then
    echo "Missing required arguments"
    usage
fi

# Execute the curl command
curl --header "PRIVATE-TOKEN: $ACCESS_TOKEN" "https://gitlab.gbt.sh/api/v4/projects/$PROJECT_ID/feature_flags" > "$OUTPUT_FILE"

