#!/bin/bash

# Internal variable for GitLab Base URL
GITLAB_BASE_URL="gitlab.com"

# Function to display usage
usage() {
    echo "Usage: $0 -t <token> -p <project_id>"
    exit 1
}

# Parsing command-line options
while getopts 't:p:' flag; do
    case "${flag}" in
        t) token="${OPTARG}" ;;
        p) project_id="${OPTARG}" ;;
        *) usage ;;
    esac
done

# Check if both token and project_id are provided
if [ -z "${token}" ] || [ -z "${project_id}" ]; then
    echo "Both token and project ID are required."
    usage
fi

# The cURL command
curl --header "PRIVATE-TOKEN:${token}" "https://${GITLAB_BASE_URL}/api/v4/projects/${project_id}/variables" > gitlab_${project_id}_variables.json

