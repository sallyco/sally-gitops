#!/bin/bash

# Internal variable for GitLab Base URL
GITLAB_BASE_URL="gitlab.com"

# Function to display usage
usage() {
    echo "Usage: $0 -t <token> -p <project_id> -f <variables_file>"
    exit 1
}

# Parsing command-line options
while getopts 't:p:f:' flag; do
    case "${flag}" in
        t) TOKEN="${OPTARG}" ;;
        p) PROJECT_ID="${OPTARG}" ;;
        f) VARIABLES_FILE="${OPTARG}" ;;
        *) usage ;;
    esac
done

# Check if token, project_id, and variables_file are provided
if [ -z "${TOKEN}" ] || [ -z "${PROJECT_ID}" ] || [ -z "${VARIABLES_FILE}" ]; then
    echo "Token, Project ID, and Variables File are required."
    usage
fi

# Read the variables file and post each variable to GitLab
jq -c '.[]' "${VARIABLES_FILE}" | while read -r i; do
    curl --request POST --header "PRIVATE-TOKEN: ${TOKEN}" \
         --header "Content-Type: application/json" \
         --data "${i}" \
         "https://${GITLAB_BASE_URL}/api/v4/projects/${PROJECT_ID}/variables"
done

