#!/bin/bash

# Check if both arguments are provided
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 VAULT_ADDR VAULT_TOKEN"
    exit 1
fi


commands=$(cat <<EOF
# Assigning the first and second argument to VAULT_ADDR and VAULT_TOKEN
export VAULT_ADDR=$1
export VAULT_TOKEN=$2

echo '- Create the prod-sally policy file inside the pod /tmp folder'
cat > /tmp/prod-sally-policy.hcl <<VAULT_EOF
path "environment/prod/*" {
capabilities = ["read"]
}
path "environment/data/prod/*" {
capabilities = ["read"]
}
VAULT_EOF

echo '- Enable the Kubernetes authentication method'
vault auth enable -path auth-mount kubernetes

echo '- Configure the Kubernetes authentication method to use the location of the Kubernetes API'
vault write auth/auth-mount/config kubernetes_host="https://\$KUBERNETES_PORT_443_TCP_ADDR:443"

echo '- Enable the kv v2 Secrets Engine.'
vault secrets enable -path=environment kv-v2

echo '- Write out the policy named prod-sally-policy that enables the read capability for secrets at all paths under environment'
 vault policy write prod-sally-policy /tmp/prod-sally-policy.hcl

echo '- Create a Kubernetes authentication role named prod-sally-role'
vault write auth/auth-mount/role/prod-sally-role \
      bound_service_account_names=default \
      bound_service_account_namespaces=production \
      policies=prod-sally-policy \
      ttl=24h
EOF
)

kubectl exec -it vault-0 -n vault -- /bin/sh -c "$commands"
