resource "aws_iam_role" "eks_route53_role" {
  name = "eks-route53-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "eks.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_policy" "eks_route53_policy" {
  name        = "eks-route53-policy"
  description = "Policy to allow EKS to modify Route53 records for ACME DNS-01 challenge"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = [
          "route53:GetChange",
          "route53:ChangeResourceRecordSets",
          "route53:ListResourceRecordSets"
        ],
        Effect   = "Allow",
        Resource = "*"
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "eks_route53_attachment" {
  role       = aws_iam_role.eks_route53_role.name
  policy_arn = aws_iam_policy.eks_route53_policy.arn
}

resource "kubernetes_manifest" "clusterissuer" {
  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind       = "ClusterIssuer"
    metadata = {
      name = "letsencrypt-clusterissuer"
    }
    spec = {
      acme = {
        server = "https://acme-v02.api.letsencrypt.org/directory"
        email  = "accounts@sally.co"
        privateKeySecretRef = {
          name = "letsencrypt-clusterissuer-key"
        }
        solvers = [{
          dns01 = {
            route53 = {
              region = var.region
            }
          }
        }]
      }
    }
  }
}
