# ArgoCD on Amazon EKS

This tutorial guides you through deploying an Amazon EKS cluster with addons configured via ArgoCD, employing the [GitOps Bridge Pattern](https://github.com/gitops-bridge-dev).

<img src="https://raw.githubusercontent.com/aws-ia/terraform-aws-eks-blueprints/main/patterns/gitops/getting-started-argocd/static/gitops-bridge.drawio.png" width=100%>

The [GitOps Bridge Pattern](https://github.com/gitops-bridge-dev) enables Kubernetes administrators to utilize Infrastructure as Code (IaC) and GitOps tools for deploying Kubernetes Addons and Workloads. Addons often depend on Cloud resources that are external to the cluster. The configuration metadata for these external resources is required by the Addons' Helm charts. While IaC is used to create these cloud resources, it is not used to install the Helm charts. Instead, the IaC tool stores this metadata either within GitOps resources in the cluster or in a Git repository. The GitOps tool then extracts these metadata values and passes them to the Helm chart during the Addon installation process. This mechanism forms the bridge between IaC and GitOps, hence the term "GitOps Bridge."

Additional examples available on the [GitOps Bridge Pattern](https://github.com/gitops-bridge-dev):

- [argocd-ingress](https://github.com/gitops-bridge-dev/gitops-bridge/tree/main/argocd/iac/terraform/examples/eks/argocd-ingress)
- [aws-secrets-manager](https://github.com/gitops-bridge-dev/gitops-bridge/tree/main/argocd/iac/terraform/examples/eks/aws-secrets-manager)
- [crossplane](https://github.com/gitops-bridge-dev/gitops-bridge/tree/main/argocd/iac/terraform/examples/eks/crossplane)
- [external-secrets](https://github.com/gitops-bridge-dev/gitops-bridge/tree/main/argocd/iac/terraform/examples/eks/external-secrets)
- [multi-cluster/distributed](https://github.com/gitops-bridge-dev/gitops-bridge/tree/main/argocd/iac/terraform/examples/eks/multi-cluster/distributed)
- [multi-cluster/hub-spoke](https://github.com/gitops-bridge-dev/gitops-bridge/tree/main/argocd/iac/terraform/examples/eks/multi-cluster/hub-spoke)
- [multi-cluster/hub-spoke-shared](https://github.com/gitops-bridge-dev/gitops-bridge/tree/main/argocd/iac/terraform/examples/eks/multi-cluster/hub-spoke-shared)
- [private-git](https://github.com/gitops-bridge-dev/gitops-bridge/tree/main/argocd/iac/terraform/examples/eks/private-git)

## Prerequisites

Before you begin, make sure you have the following command line tools installed:

- git
- terraform
- kubectl
- argocd

## (Optional) Fork the GitOps git repositories

See the appendix section [Fork GitOps Repositories](#fork-gitops-repositories) for more info on the terraform variables to override.

## GitLab Registry Docker Container access
We will need to create a Kubernetes Secret with the `.dockerconfigjson` key and value to allow ArgoCD to pull Sally's images from our private docker registry.

In order to do so, create a repository token called `gitlab-registry` with `api` rights, via command line connect to the private registry via command below.
```bash
docker login registry.gitlab.com -u accounts@sally.co -p <token>
```
If the login succeeded you should see something like this
```text
WARNING! Using --password via the CLI is insecure. Use --password-stdin.
Login Succeeded
```

View the config.json file:
```bash
cat ~/.docker/config.json
```

The output contains a section similar to this:
```json
{
    "auths": {
        "registry.gitlab.com": {
            "auth": "encoded_credentials"
        }
    }
}
```

If `encoded_credentials` is missing, follow steps 1 and 2, otherwise skip them
1. Create Credential String:

Manually create the credential string in the format `username:token`.

2. Base64 Encode:

Encode this string to base64. On Unix-based systems, you can use:
```bash
echo -n 'username:token' | base64
```

3. Base64 Encode Correctly

Make sure you're encoding the entire JSON object above into base64, not just the `username:token` part. On Unix-based systems, you can do this using:
```bash
echo -n '{"auths": {"registry.gitlab.com": {"auth": "encoded_credentials"}}}' | base64
```

4. Replace the result of the previous command in the value of the `.dockerconfigjson` key in this terraform file [argocd-gitlab.tf](./argocd-gitlab.tf) before you continue, otherwise the Kubernetes Secret will not be created correctly.


For more detail, see the documentation on how to [Pull an Image from a Private Registry](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/).

## Deploy the EKS Cluster

Setup aws cli with profile named `sally` and make sure the IAM principal has access to development and production environments
```bash
aws configure --profile sally
```
Setup the env variables for AWS profile and region
```bash
export AWS_PROFILE=sally 
export AWS_REGION=us-east-1 
```

Initialize Terraform and deploy the EKS cluster:

```bash
terraform init
terraform apply -target="module.vpc" -auto-approve
terraform apply -target="module.eks" -auto-approve
terraform apply \
  -target="module.gitops_bridge_bootstrap" \
  -target="module.eks_blueprints_addons" \
  -target="module.ebs_csi_driver_irsa" \
  -target="data.aws_route53_zone.this" \
  -auto-approve
```

To retrieve `kubectl` config, execute the terraform output command:

```shell
terraform output -raw configure_kubectl
```

The expected output will have two lines you run in your terminal

```text
export KUBECONFIG="/tmp/sally"
aws eks --region us-east-1 update-kubeconfig --name sally
```

>The first line sets the `KUBECONFIG` environment variable to a temporary file
that includes the cluster name. The second line uses the `aws` CLI to populate
that temporary file with the `kubectl` configuration. This approach offers the
advantage of not altering your existing `kubectl` context, allowing you to work
in other terminal windows without interference.

Terraform will add GitOps Bridge Metadata to the ArgoCD secret.
The annotations contain metadata for the addons' Helm charts and ArgoCD ApplicationSets.

```shell
kubectl get secret -n argocd -l argocd.argoproj.io/secret-type=cluster -o json | jq '.items[0].metadata.annotations'
```

The output looks like the following:

```json
{
  "addons_repo_basepath": "argocd/",
  "addons_repo_path": "bootstrap/control-plane/addons",
  "addons_repo_revision": "main",
  "addons_repo_url": "https://gitlab.com/sallyco/eks-blueprints-add-ons.git",
  "argocd_hosts": "[argocd.sally.co]",
  "aws_account_id": "360217847587",
  "aws_cluster_name": "sally",
  "aws_load_balancer_controller_iam_role_arn": "arn:aws:iam::360217847587:role/alb-controller-20240111221014111200000012",
  "aws_load_balancer_controller_namespace": "kube-system",
  "aws_load_balancer_controller_service_account": "aws-load-balancer-controller-sa",
  "aws_region": "us-east-1",
  "aws_vpc_id": "vpc-09f3f5883b5acb6ad",
  "cert_manager_iam_role_arn": "arn:aws:iam::360217847587:role/cert-manager-20240111221014111300000013",
  "cert_manager_namespace": "cert-manager",
  "cert_manager_service_account": "cert-manager",
  "cluster_autoscaler_iam_role_arn": "arn:aws:iam::360217847587:role/cluster-autoscaler-20240111221014111200000011",
  "cluster_autoscaler_namespace": "kube-system",
  "cluster_autoscaler_service_account": "cluster-autoscaler-sa",
  "cluster_name": "in-cluster",
  "environment": "prod",
  "external_dns_domain_filters": "[sally.co]",
  "external_dns_iam_role_arn": "arn:aws:iam::360217847587:role/external-dns-20240111221014112000000015",
  "external_dns_namespace": "external-dns",
  "external_dns_service_account": "external-dns-sa",
  "karpenter_iam_role_arn": "arn:aws:iam::360217847587:role/karpenter-20240111221014111900000014",
  "karpenter_namespace": "karpenter",
  "karpenter_node_iam_role_name": "karpenter-sally-20240111220943773100000009",
  "karpenter_node_instance_profile_name": "karpenter-sally-2024011122094421260000000b",
  "karpenter_service_account": "karpenter",
  "karpenter_sqs_queue_name": "karpenter-sally",
  "workload_repo_path": "k8s",
  "workload_repo_revision": "main",
  "workload_repo_url": "https://gitlab.com/sallyco/sally-gitops.git"
}
```

The labels offer a straightforward way to enable or disable an addon in ArgoCD for the cluster.

```shell
kubectl get secret -n argocd -l argocd.argoproj.io/secret-type=cluster -o json | jq '.items[0].metadata.labels' | grep -v false | jq .
```

The output looks like the following:

```json
{
  "argocd.argoproj.io/secret-type": "cluster",
  "aws_cluster_name": "sally",
  "cluster_name": "in-cluster",
  "enable_argocd": "true",
  "enable_aws_argocd_ingress": "true",
  "enable_aws_load_balancer_controller": "true",
  "enable_cert_manager": "true",
  "enable_cluster_autoscaler": "true",
  "enable_external_dns": "true",
  "enable_karpenter": "true",
  "enable_kube_prometheus_stack": "true",
  "enable_metrics_server": "true",
  "environment": "prod",
  "kubernetes_version": "1.28"
}

```

## Deploy the Addons

Bootstrap the addons using ArgoCD:

```shell
kubectl apply -f bootstrap/addons.yaml
```

### Monitor GitOps Progress for Addons

Wait until all the ArgoCD applications' `HEALTH STATUS` is `Healthy`.
Use `Ctrl+C` or `Cmd+C` to exit the `watch` command. ArgoCD Applications
can take a couple of minutes in order to achieve the Healthy status.

```shell
kubectl get applications -n argocd -w
```

The expected output should look like the following:

```text
addon-in-cluster-argo-cd                        OutOfSync     Healthy
addon-in-cluster-aws-argo-cd                    Synced        Healthy
addon-in-cluster-aws-load-balancer-controller   Synced        Healthy
addon-in-cluster-cert-manager                   Synced        Healthy
addon-in-cluster-cluster-autoscaler             Synced        Healthy
addon-in-cluster-external-dns                   Synced        Healthy
addon-in-cluster-karpenter                      Synced        Healthy
addon-in-cluster-kube-prometheus-stack          Synced        Healthy
addon-in-cluster-metrics-server                 Synced        Healthy
cluster-addons                                  Synced        Healthy
```

### Verify the Addons

Verify that the addons are ready:

```shell
kubectl get deployment -n kube-system \
  aws-load-balancer-controller \
  metrics-server
kubectl get deployment -n cert-manager \
  cert-manager \
  cert-manager-cainjector \
  cert-manager-webhook
kubectl get deployment -n kube-system \
  cluster-autoscaler-aws-cluster-autoscaler
kubectl get deployment -n argocd \
  argo-cd-argocd-applicationset-controller \
  argo-cd-argocd-repo-server \
  argo-cd-argocd-server
kubectl get deployment -n karpenter \
  karpenter
kubectl get deployment -n external-dns \
  external-dns  
kubectl get deployment -n kube-prometheus-stack \
  kube-prometheus-stack-grafana \
  kube-prometheus-stack-kube-state-metrics \
  kube-prometheus-stack-operator
```

The expected output should look like the following:

```text
NAME                                       READY   UP-TO-DATE   AVAILABLE   AGE
aws-load-balancer-controller               2/2     2            2           7m21s
metrics-server                             1/1     1            1           7m41s
cert-manager                               1/1     1            1           25m
cert-manager-cainjector                    1/1     1            1           25m
cert-manager-webhook                       1/1     1            1           25m
cluster-autoscaler-aws-cluster-autoscaler  1/1     1            1           44m
argo-cd-argocd-applicationset-controller   1/1     1            1           109m
argo-cd-argocd-repo-server                 1/1     1            1           109m
argo-cd-argocd-server                      1/1     1            1           109m
karpenter                                  2/2     2            2           26m
external-dns                               1/1     1            1           9m25s
kube-prometheus-stack-grafana              1/1     1            1           26m
kube-prometheus-stack-kube-state-metrics   1/1     1            1           26m
kube-prometheus-stack-operator             1/1     1            1           26m
```

## Access ArgoCD

Access to the ArgoCD's UI to make sure it's working properly, if you want to do it,
run the commands shown in the Terraform output as the example below:

```shell
terraform output -raw access_argocd
```

The expected output should contain the `kubectl` config followed by `kubectl` command to retrieve
the URL, username, password to login into ArgoCD UI or CLI.

```text
echo "ArgoCD Username: admin"
echo "ArgoCD Password: $(kubectl get secrets argocd-initial-admin-secret -n argocd --template="{{index .data.password | base64decode}}")"
echo "ArgoCD URL: https://$(kubectl get svc -n argocd argo-cd-argocd-server -o jsonpath='{.status.loadBalancer.ingress[0].hostname}')"
```

### Create `gitlab` user in ArgoCD
This user will be used to run `argocd` commands in GitLab Pipeline for deployment in production.

#### Manually Adding a User to the argocd-cm ConfigMap
Based on the ArgoCD documentation, you can add a new user to the argocd-cm ConfigMap like this:

1. Edit the ConfigMap:
First login to ArgoCD with the admin credentials

```bash
argocd login argocd.sally.co --username admin --password <password> --insecure
```

Use `kubectl` to edit the `argocd-cm` ConfigMap:

```bash
kubectl edit configmap argocd-cm -n argocd
```

2. Add a User Account:

In the editor, add the following lines under the data section, right before `admin.enabled: "true"`:
```yaml
accounts.gitlab: apiKey, login
```
The result will look like this
```txt
.
.
apiVersion: v1
data:
  accounts.gitlab: apiKey
  admin.enabled: "true"
.
.

```

apiKey enables API key generation, and login enables UI login.

3. Save and Exit:
Save the changes and exit the editor. This will update the ConfigMap.

4. Generate a Token:

After adding the user, generate a token using the ArgoCD CLI:
```bash
argocd account generate-token --account gitlab
```

Use this token in the `ARGOCD_TOKEN_PRODUCTION` CI/CD environment variable

## Semantic Release
The [Semantic Release](https://semantic-release.gitbook.io/semantic-release/) plugin is used to automate the release process.
Don't forget to update the REGISTRY_PASSWORD variable in the CI/CD pipeline with a valid token, since they expire every year.

## Deploy Sally's dependencies

Initialize Terraform and deploy all Sally's dependencies:

```bash
terraform init
terraform plan
terraform apply -auto-approve
```

## Enable Hashicorp Vault
HashiCorp Vault is a robust tool for securely accessing secrets, like passwords and API keys, in a centralized way. It helps manage and protect sensitive data, ensuring only authorized users and applications can access it. See the complete [Vault Documentation](https://developer.hashicorp.com/vault/docs).

We are deploying Vault using `ha` ([High Availability](https://developer.hashicorp.com/vault/docs/internals/high-availability)) with 2 nodes and `awskms` ([Auto unsealing](https://developer.hashicorp.com/vault/docs/configuration/seal/awskms)) mode.

In order to make Vault operational we need to go through the process of initialization. Please follow the steps below:

1. List all Vault pods to make sure they are `Running`
```shell
 kubectl get pods -n vault -l app.kubernetes.io/name=vault        
 ```
 The expected result should look like this
 ```text
 NAME      READY   STATUS    RESTARTS   AGE
vault-0   0/1     Running   0          2m30s
vault-1   0/1     Running   0          2m29s
```
```shell
kubectl exec --stdin=true --tty=true vault-0 -n vault -- vault status
```
> ℹ **Note:** They are not ready yet, by default Vault first initialization looks like this, not initialized and sealed.

```text
Key                      Value
---                      -----
Recovery Seal Type       awskms
Initialized              false
Sealed                   true
Total Recovery Shares    0
Threshold                0
Unseal Progress          0/0
Unseal Nonce             n/a
Version                  1.15.2
Build Date               2023-11-06T11:33:28Z
Storage Type             raft
HA Enabled               true
command terminated with exit code 2
```

2. To initialize Vault, run the command below for `vault-0` node only.
```shell
kubectl exec --stdin=true --tty=true vault-0 -n vault -- vault operator init
```
 The expected result should look like this
 ```text
Recovery Key 1: ............................................
Recovery Key 2: ............................................
Recovery Key 3: ............................................
Recovery Key 4: ............................................
Recovery Key 5: ............................................

Initial Root Token: hvs.XyzxYzxyZxyzXyZxYzXyzxyzx


Success! Vault is initialized

Recovery key initialized with 5 key shares and a key threshold of 3. Please
securely distribute the key shares printed above.
```
> ⚠️ **Important:** And more importantly, don't forget to store the `Initial Root Token` and `Recovery keys` somewhere safe.

Because we are using `awskms` auto unsealing method and we are also using [`retry_join`](https://developer.hashicorp.com/vault/tutorials/raft/raft-storage-aws#retry-join) block for both nodes inside the storage raft, we don't need to go trough the process of unsealing each node of the Vault cluster.

To access the Vault UI in your browser, visit [vault.sally.co](https://vault.sally.co) and enter `Initial Root Token` from the initialization step.

## Configure Hashicorp Vault via script
Injecting secrets into Kubernetes pods via Vault Agent containers. See the [oficial documentation](https://developer.hashicorp.com/vault/tutorials/kubernetes/kubernetes-sidecar) for more detail.

This is needed to inject the Vault agent and init into the api-core and operations-ui pods.

1. Run the script `./config/vault/config.sh $VAULT_ADDR $VAULT_TOKEN` like below
```bash
./config/vault/config.sh "http://127.0.0.1:8200" "<token>
```
Don't forget to replace the `<token>` by the one you from the unsealing process

2. The result should look like this
```
- Create the prod-sally policy file inside the pod /tmp folder
- Enable the Kubernetes authentication method
Success! Enabled kubernetes auth method at: auth-mount/
- Configure the Kubernetes authentication method to use the location of the Kubernetes API
Success! Data written to: auth/auth-mount/config
- Enable the kv v2 Secrets Engine.
Success! Enabled the kv-v2 secrets engine at: environment/
- Write out the policy named prod-sally-policy that enables the read capability for secrets at all paths under environment
Success! Uploaded policy: prod-sally-policy
- Create a Kubernetes authentication role named prod-sally-role
Success! Data written to: auth/auth-mount/role/prod-sally-role
```
> ⚠️ **Important:** In case you are recreating Vault due to a disaster don't forget to follow the steps in the VSO (Vault Secrets Operator) section, otherwise the secrets will not be injected into the pods.

## Vault secrets migration for Sally
If you need to migrate secrets from the some other Vault instance, first connect to both Vaults `source` and `target`. On the `target` create a `kv` `Enable a Secrets Engine` called `environment`, inside of it create a secret called `prod/api-core`, on the source select this secret, enable JSON format, copy the content and place it to the `target`. Repeat the process for secrets `prod/api-core-v2` and `prod/operations-ui-v2`.

## Vault Backup Strategy
HashiCorp Vault data is automatically backed up using AWS Backup service. The backup configuration includes:

- Daily backups at 5 AM UTC
- 30-day retention period
- Dedicated AWS Backup vault
- Tag-based volume selection
- Automated backup management through IAM roles

The backup strategy ensures business continuity by preserving Vault's critical data, including:
- Encryption keys
- Authentication configurations
- Secrets and policies
- Access control settings

In case of disaster recovery, the backed-up EBS volumes can be restored to recover the Vault cluster's state.

## Vault Disaster Recovery
In case of a disaster, follow these steps to restore Vault:

1. Restore the EBS volume from AWS Backup:
```bash
aws backup restore-backup --backup-id <backup-id> --iam-role-arn <vault-backup-role-arn>
```

2. Delete existing Vault resources:
```bash
kubectl delete statefulset vault -n vault
kubectl delete pvc data-vault-0 data-vault-1 -n vault
```

3. Update the Vault helm values to use the restored volume ID:
```yaml
server:
  dataStorage:
    storageClass: "gp2"
    volumeId: "<restored-volume-id>"
```

4. Redeploy Vault:
```terraform
terraform apply -target="helm_release.vault" -auto-approve
```

5. Verify Vault is operational:
```bash
kubectl get pods -n vault
kubectl exec -n vault vault-0 -- vault status
```

This addition provides clear step-by-step instructions for disaster recovery procedures.


## Vault secrets syncronization with Kubernetes secrets via VSO (Vault Secrets Operator)
Vault Secrets Operator is a tool designed for managing sensitive data in Kubernetes environments. It stands out from Vault Agent Injector by offering a more efficient way of handling secrets, particularly in terms of resource consumption. Unlike Vault Agent Injector, which injects secrets directly into pods, Vault Secrets Operator creates Kubernetes Secrets that pods can use, leading to less overhead and more streamlined secret management. This approach not only reduces the resource usage on Kubernetes clusters but also simplifies secret rotation and management, making it a more efficient and scalable solution for handling secrets in Kubernetes.

Here are some good referentes and documentation:
- [Vault Secrets Operator](https://developer.hashicorp.com/vault/docs/platform/k8s/vso/sources/vault)
- [Vault Secrets Operator for Kubernetes now GA](https://www.hashicorp.com/blog/vault-secrets-operator-for-kubernetes-now-ga?utm_source=YouTube&utm_medium=video&utm_campaign=24Q3_NewReleases&utm_content=VSOk8s&utm_offer=blog)
- [Kubernetes Vault integration via Sidecar Agent Injector vs. Vault Secrets Operator vs. CSI provider](https://www.hashicorp.com/blog/kubernetes-vault-integration-via-sidecar-agent-injector-vs-csi-provider)

Now we need to create the necessary CRDs to make VSO work
1. `VaultAuth`: VaultAuth CRDs provide Vault authentication configuration information for the operator. Consider VaultAuth as foundational resources used by all secret replication type resources.
2. `VaultStaticSecret`: Provides the configuration necessary for the Operator to synchronize a single Vault static Secret to a single Kubernetes Secret. Supported secrets engines: kv-v2, kv-v1

Run the commands below to create the CRDs. Make sure you are in the root folder before running it.
```bash
kubectl apply -f ./config/vault/vso/vault-auth.yaml
kubectl apply -f ./config/vault/vso/vss-api-core.yaml
kubectl apply -f ./config/vault/vso/vss-api-core-v2.yaml
kubectl apply -f ./config/vault/vso/vss-operations-ui-v2.yaml
```

> ⚠️ **Important:** Since we are storing sensitive and important data, even after running `./destroy.sh` script of `terraform destroy`, the `PVC`s attached to the vault pods will not be deleted. If you want to delete them, you'll need to do manually for security reasons. Don't forget to delete those VSO CRDs `VaultAuth` and `VaultStaticSecret`.


## (Optional) Access Prometheus Grafana

Access to the Prometheus Grafana UI is completely optional, if you want to do it,
run the commands shown in the Terraform output as the example below:

```shell
terraform output -raw access_prometheus_grafana
```

The expected output should contain the `kubectl` config followed by `kubectl` command to retrieve
the URL, username, password to login into PRometheus Grafana UI.

```text
echo "Prometheus Grafana Username: $(kubectl get secret kube-prometheus-stack-grafana -n kube-prometheus-stack -o jsonpath="{.data.admin-user}" | base64 --decode)"
echo "Prometheus Grafana Password: $(kubectl get secret kube-prometheus-stack-grafana -n kube-prometheus-stack -o jsonpath="{.data.admin-password}" | base64 --decode)"
```
In order to access the Prometheus Grafana UI in your browser, port-forward it using the command below.
```
kubectl port-forward service/kube-prometheus-stack-grafana -n kube-prometheus-stack 3000:80
```

Visit localhost:3000 in your web browser and enter the root user credentials.


## (Optional) Access MinIO
MinIO is being used by Sally as S3 bucket gateway.

Access to the MinIO UI is completely optional, if you want to do it,
run the commands shown in the Terraform output as the example below:

```shell
terraform output -raw access_minio
```

The expected output should contain the `kubectl` config followed by `kubectl` command to retrieve the secret and access keys to login into MinIO UI or CLI.

In order to access the MinIO UI in your browser, port-forward it using the command below.
```
kubectl port-forward service/minio-service -n production 9001:9001
```

Visit localhost:9001 in your web browser and enter the root user credentials. Don't use minio.sally.co url because it's failing to log in.

## (Optional) Access Keycloak
Access to the Keycloak UI is completely optional, if you want to do it,
run the commands shown in the Terraform output as the example below:

```shell
terraform output -raw access_keycloak
```

The expected output should contain the `kubectl` config followed by `kubectl` command to retrieve the secret and access keys to login into Keycloak UI or CLI.

In order to access the Keycloak UI in your browser, visit [auth.sally.co](https://auth.sally.co) and enter the admin user credentials.

> ⚠️ **Important:** Since we are storing important data, even after running `./destroy.sh` script of `terraform destroy`, the `PostregreSQL` database will not be deleted due to the `deletion_protection` parameter. If you want to delete it, you'll need to do it manually for security reasons.

<!--## (Optional) Access RabbitMQ-->
<!---->
<!--**What is RabbitMQ?** RabbitMQ is a software that lets different parts of an application send messages to each other. It's like a post office for the app, where different parts can drop off or pick up messages, helping them communicate efficiently.-->
<!---->
<!--**Why Use RabbitMQ**? It's useful because it makes sure messages don't get lost, even if parts of the app are busy or temporarily down. This helps in keeping the app running smoothly and coordinating tasks, especially when different parts of the app are spread out or need to talk to each other a lot.-->
<!---->
<!--Access to the RabittMQ UI is completely optional, if you want to do it,-->
<!--run the commands shown in the Terraform output as the example below:-->
<!---->
<!--```shell-->
<!--terraform output -raw access_rabbitmq-->
<!--```-->
<!---->
<!--The expected output should contain the `kubectl` config followed by `kubectl` command to retrieve the secret and access keys to login into RabbitMQ UI.-->
<!---->
<!--```text-->
<!--echo "RabittMQ Username: admin"-->
<!--echo "RabittMQ Password: $(kubectl get secrets argocd-initial-admin-secret -n argocd --template="{{index .data.password | base64decode}}")"-->
<!--echo "RabittMQ URL: https://$(kubectl get svc -n argocd argo-cd-argocd-server -o jsonpath='{.status.loadBalancer.ingress[0].hostname}')"-->
<!--```-->
<!---->
<!--> ⚠️ **Important:** Since we assincronously managing communication in applications where losing any message is not an option, even after running `./destroy.sh` script of `terraform destroy`, the correspondent `PVC` will not be deleted. If you want to delete it, you'll need to do it manually for security reasons.-->
<!---->
## (Optional) Access Bull Board
**What is Bull Board?** Bull Board is a web-based UI that allows you to monitor and manage your Bull or BullMQ queues. It provides a user-friendly interface to visualize the state of your queues, inspect jobs, and perform actions like retrying or deleting jobs. This can be incredibly helpful for debugging and ensuring that your background job processing is running smoothly.

**Why Use Bull Board?** In applications that rely on background processing, having visibility into your job queues is crucial. Bull Board helps you:

* Monitor Queue Activity: See the number of jobs in each state (waiting, active, completed, failed).
* Inspect Job Details: View individual job data, including payload and metadata.
* Manage Jobs: Retry failed jobs, delete completed jobs, or promote delayed jobs.
* Access to the Bull Board UI is completely optional. If you want to access it, you can follow the steps below.

**Accessing Bull Board UI:**
To retrieve the access information for Bull Board, run the following command:
```shell
terraform output -raw access_bull_board
```
The expected output should contain the kubectl commands to retrieve the username and password to log in to the Bull Board UI.

Example Output:
```text
echo "Bull Board Username: $(kubectl get secret bullboard-secret -n production -o jsonpath='{.data.BULLBOARD_USER}' | base64 --decode)"
echo "Bull Board Password: $(kubectl get secret bullboard-secret -n production -o jsonpath='{.data.BULLBOARD_PASSWORD}' | base64 --decode)"
echo "Bull Board URL: https://bullboard.sally.co/admin/queues"
```

**Steps to Access:**
1. Retrieve Credentials:
Run the following commands to get your Bull Board username and password:

```shell
echo "Bull Board Username: $(kubectl get secret bullboard-secret -n production -o jsonpath='{.data.BULLBOARD_USER}' | base64 --decode)"
echo "Bull Board Password: $(kubectl get secret bullboard-secret -n production -o jsonpath='{.data.BULLBOARD_PASSWORD}' | base64 --decode)"
```
This will output your username and password. Keep these credentials secure.


2. Access the Bull Board UI:
Open your web browser and navigate to:
```text
https://bullboard.sally.co/admin/queues
```

2. Login:

* Username: Use the username retrieved in step 1.
* Password: Use the password retrieved in step 1.

4. Explore Your Queues:

Once logged in, you can:

* **Monitor Queue Status:** View the number of jobs in various states.
* **Inspect Jobs:** Click on individual jobs to see their details.
* **Manage Jobs:** Retry, promote, or delete jobs as needed.

**Troubleshooting:**

* **Access Issues:** If you cannot access the UI, ensure that the ingress resource for Bull Board is correctly configured, and that the service is running.
* **Invalid Credentials:** If you're unable to log in, double-check that the credentials are correct and that the Kubernetes secret bullboard-secret contains the correct values.
* **SSL/TLS Configuration:** Make sure that TLS is properly set up if you're accessing the UI over HTTPS.

**Security Considerations:**
* **Protect Your Credentials:** Do not share your Bull Board username and password publicly.
* **Use HTTPS:** Always access the UI over HTTPS to encrypt your connection and protect your credentials.
* **Restrict Access:** Consider implementing additional security measures, such as IP whitelisting or integrating with an authentication provider, to restrict who can access the Bull Board UI.


## (Optional) Access Camunda

**Camunda in Simple Terms:**

**Like a Traffic Controller for Business Processes:** Imagine Camunda as a traffic controller at a busy intersection. In a company, there are many tasks and workflows (like cars on roads) that need to be managed and directed smoothly. Camunda helps in controlling these workflows, ensuring they follow the right path without crashing into each other.
Key Features:

**Workflow and Decision Automation:** It automates business processes (like approving a loan, handling customer requests) and decisions (like determining if a customer is eligible for a loan).
Visual Workflow Modeling: You can draw your business process, like drawing a map, which makes it easier to understand and follow.
Monitoring and Maintenance: It keeps an eye on processes, showing where they are and if there are any problems, much like a GPS tracking system.

**Benefits:**

**Efficiency:** Automating processes saves time and reduces errors.
**Flexibility:** Can be changed easily to adapt to new business needs.
**Transparency:** Everyone can see how processes are running, like having a clear map of all business activities.

Access to the Camunda UI is completely optional, if you want to do it,
run the commands shown in the Terraform output as the example below:

```shell
terraform output -raw access_camunda
```

The expected output should contain the `kubectl` config followed by `kubectl` command to retrieve the secret and access keys to login into Camunda UI.

```text
aws eks --region us-east-1 update-kubeconfig --name sally
echo "Camunda Username: $(kubectl get secret camunda-admin-creds -n production -o jsonpath="{.data.username}" | base64 --decode)"
echo "Camunda Password: $(kubectl get secret camunda-admin-creds -n production -o jsonpath="{.data.password}" | base64 --decode)"
echo "Camunda URL: https://$(kubectl get ing -n production camunda-bpm-platform -o jsonpath='{.spec.rules[0].host}')/camunda"
```

In order to access the Camunda UI in your browser, visit [camunda.sally.co/camunda](https://camunda.sally.co/camunda) and enter the user credentials, if it doesn't work, use demo/demo as username and password. It's failing admin login, but it works with demo/demo.


## (Optional) Access AWS DocumentDB from MongoDB Compass

Document DB is the official Sally's non-relational database. It is deployed in private subnet for security reasons.

Access to production DocumentDB is completely optional, if you want to do it,
run the commands shown in the Terraform output as the example below:

```shell
terraform output -raw access_documentdb
```

The expected output should contain the `kubectl` config followed by `echo` command to retrieve the username, password and hostname to login into DocumentDB.

In order to access DocumentDB follow the instructions in AWS official documentation [Connecting to an Amazon DocumentDB Cluster from Outside an Amazon VPC](https://docs.aws.amazon.com/documentdb/latest/developerguide/connect-from-outside-a-vpc.html).

There is also a quick Youtube video that will help you to [Connect to AWS DocumentDB Cluster from outside VPC using MongoDB Compass](https://www.youtube.com/watch?v=9OQ7BrRWDWQ).

## (Optional) Access AWS RDS PostgreSQL from DBeaver

PostgreSQL is the official Sally's Keycloak relational database, it's also used by Camunda (processengine). It is deployed in private subnet for security reasons.

Access to production PostgreSQL is completely optional, if you want to do it,
run the commands shown in the Terraform output as the example below:

```shell
terraform output -raw access_postgresql
```

The expected output should contain the `kubectl` config followed by `echo` command to retrieve the username, password and hostname to login into DocumentDB.

In order to access PostgreSQL follow the instructions in AWS official documentation [Connecting to an Amazon DocumentDB Cluster from Outside an Amazon VPC](https://docs.aws.amazon.com/documentdb/latest/developerguide/connect-from-outside-a-vpc.html). Although the documentation is meant for DocumentDB, the SSH tunnelling apply for all databases. Make sure the PostgreSQL security group allows `PostgreSQL` type on port `5432` from our `Bastion Host` which is currently running on this subnet 10.0.48.0/24.


## Database Seeder 2
Follow the instructions below to create a secret for Keycloak `admin-cli`

Generating Keycloak Client Secret for admin-cli
To generate a client secret for the admin-cli client in Keycloak, follow these steps:

1. Access Keycloak Admin Console

 - URL: Navigate to the Keycloak Admin Console
 - Login: Use your admin credentials to log in.
2. Select the `master` Realm
 - Realm: Choose the appropriate realm from the drop-down menu at the top left.
3. Access Clients Section
 - Clients: In the menu on the left, click on "Clients".
4. Find admin-cli Client
 - Search: Look for admin-cli in the list of clients. Click on it.
5. Change Access Type
- Access Type: Change the `Access Type` to confidential. This allows a secret to be generated.
6. Save Changes
- Save: Click "Save" at the bottom of the page.
7. Generate Secret
- Credentials Tab: Go to the `Credentials` tab for the admin-cli client.
- Regenerate Secret: Click "Regenerate Secret" to create a new secret.
- Copy Secret: Copy the generated secret. This is what you'll use as the client secret.

### Update the `helm.sh` file in database-seeder repo

Use this secret in `KC_AUTH_SECRET` variable in [database-seeder-2](https://gitlab.com/sallyco/devops/database-seeder-2). For the Keycloak `KC_REALM_PASS` variable use the admin password. For the `MINIO_ACCESS_KEY` use the MinIO user, for the `MINIO_SECRET_KEY` use the MinIO password. For the `MONGODB_URL` use this URI `mongodb://<docdb username>:<docdb password>@<docdb hostname>:<docdb port>/?tls=true&tlsCAFile=/etc/mongo/sally-rds-combined-ca-bundle.pem&replicaSet=rs0&readPreference=secondaryPreferred&retryWrites=false`, the `<docdb password>` make sure it is percent-enconded.

Run `./helm.sh` in database-seeder-2 to manually depoy it on the cluster.

## Feature flags setup
Feature Flags in GitLab offer a powerful way to control the roll-out of new features in software applications, allowing developers to enable or disable functionality without deploying new code. This system facilitates testing in production environments and enables a more gradual release process, enhancing both the stability and user experience. By using feature flags, teams can quickly respond to issues, perform A/B testing, and customize features for different user segments, significantly improving the agility and flexibility of the development lifecycle. See the [Feature flags](https://docs.gitlab.com/ee/operations/feature_flags.html) documentation for more details.

> ⚠️ **Important:** Make sure you enable Feature Flag in the omnibus-paas repo before you proceed with the Workload deployment

Follow the instructions in this video [Feature Flags configuration, instrumentation and use](https://www.youtube.com/watch?v=ViA6suScxkE) to configure and enable Feature Flag.

### Migrating feature flags from the [existing](https://gitlab.gbt.sh/gbt/microservices/omnibus-paas) to the [new](https://gitlab.com/sallyco/microservices/omnibus-paas) omnibu-pass repo.

1. Create an access token in the existing omnibus-pass repo and run the shell scrip as below. Don't forget to replace the placeholders
```bash
./config/feature-flags/export-ff.sh <access_token> -p <project_id> -f <output_file>
```
2. Create an access token in the new omnibus-pass repo and run the shell scrip as below. Don't forget to replace the placeholders
```bash
./import-ff.sh -t <access_token> -p <project_id> -f <json_file>
```
3. Since there are only 2 user lists (gbt and sparticle), you can them manually.

## Deploy the Workload (`api-core-v2` and `operations-ui-v2`)

Before deploying the workload, make sure to setup the GitLab Helm repo in ArgoCD with a predefinied `api-core-v2` access token. Here are the instructions:
1. Got to `settings` menu in ArgoCD
2. Choose `Repositories` option and click on `+ Connect Repo` button
3. Select `HTTPS` connection method, in type select `Helm`, in name type `Sally Helm Repo`, in project select `Default`, in repository URL type `https://gitlab.com/api/v4/projects/54297377/packages/helm/stable` make sure the project code is correct, in username type `accounts@sally.co`, and in password use a personal GitLab token `gitlab-registry` that you create under user accounts@sally.co. I already created one which it is on our password spreadsheet.

Deploy `api-core-v2` and `operations-ui-v2` using ArgoCD:

```shell
kubectl apply -f bootstrap/api-core-v2.yaml
kubectl apply -f bootstrap/operations-ui-v2.yaml
```

### Monitor GitOps Progress for Workload

Wait until all the ArgoCD applications' `HEALTH STATUS` is `Healthy`.
Use `Ctrl+C` or `Cmd+C` to exit the `watch` command. ArgoCD Applications
can take a couple of minutes in order to achieve the Healthy status.

```shell
watch kubectl get -n argocd applications api-core-v2 operations-ui-v2
```

The expected output should look like the following:

```text
NAME               SYNC STATUS   HEALTH STATUS
api-core-v2        Synced        Healthy
operations-ui-v2   Synced        Healthy
```

### Verify the Application

Verify that the `operations-ui-v2` application configuration is present and the pod is running:

```shell
  kubectl get -n production deploy,svc,ep,ing -l app.kubernetes.io/instance=operations-ui-v2
```

The expected output should look like the following:

```text
NAME                               READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/operations-ui-v2   1/1     1            1           21m

NAME                       TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)   AGE
service/operations-ui-v2   ClusterIP   172.20.20.253   <none>        80/TCP    21m

NAME                         ENDPOINTS          AGE
endpoints/operations-ui-v2   10.0.41.187:3000   21m

NAME                                         CLASS   HOSTS            ADDRESS                                                      PORTS     AGE
ingress.networking.k8s.io/operations-ui-v2   alb     *.ops.sally.co   k8s-sally-f862ecd69c-125559248.us-east-1.elb.amazonaws.com   80, 443   15d
```

Verify that the `api-core-v2` application configuration is present and the pod is running:

```shell
  kubectl get -n production deploy,svc,ep,ing -l app.kubernetes.io/instance=api-core-v2
```

The expected output should look like the following:

```text
NAME                          READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/api-core-v2   1/1     1            1           3h30m

NAME                  TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)   AGE
service/api-core-v2   ClusterIP   172.20.7.67   <none>        80/TCP    3h30m

NAME                    ENDPOINTS          AGE
endpoints/api-core-v2   10.0.10.216:3000   3h30m

NAME                                    CLASS   HOSTS          ADDRESS                                                       PORTS     AGE
ingress.networking.k8s.io/api-core-v2   alb     api.sally.co   k8s-sally-f862ecd69c-1451133475.us-east-1.elb.amazonaws.com   80, 443   3h30m
```

AWS Load Balancer can take a couple of minutes in order to be created.

Run the following command and wait until and event for ingress `operations-ui-v2` contains `Successfully reconciled`.
Use `Ctrl+C` or `Cmd+C`to exit the `watch` command.

```shell
kubectl events -n production --for ingress/operations-ui-v2 --watch
```

The expected output should look like the following:

```text
LAST SEEN   TYPE     REASON                   OBJECT              MESSAGE
11m         Normal   SuccessfullyReconciled   Ingress/operations-ui-v2   Successfully reconciled
```

Run the following command and wait until and event for ingress `api-core-v2` contains `Successfully reconciled`.
Use `Ctrl+C` or `Cmd+C`to exit the `watch` command.

```shell
kubectl events -n production --for ingress/api-core-v2 --watch
```

The expected output should look like the following:

```text
LAST SEEN   TYPE     REASON                   OBJECT              MESSAGE
11m         Normal   SuccessfullyReconciled   Ingress/api-core-v2   Successfully reconciled
```

### Access the `api-core-v2` application using Ingress

Verify the `api-core-v2` application endpoint health using `wget`:

```shell
wget -S --spider $(kubectl get -n production ingress api-core-v2 -o jsonpath='{.spec.rules[0].host}')
```

The expected output should look like the following:

```text
Spider mode enabled. Check if remote file exists.
--2024-01-23 19:40:26--  http://api.sally.co/
Resolving api.sally.co (api.sally.co)... 52.72.90.200, 184.73.179.9, 44.193.99.49
Connecting to api.sally.co (api.sally.co)|52.72.90.200|:80... connected.
HTTP request sent, awaiting response...
  HTTP/1.1 301 Moved Permanently
  Server: awselb/2.0
  Date: Wed, 24 Jan 2024 00:40:26 GMT
  Content-Type: text/html
  Content-Length: 134
  Connection: keep-alive
  Location: https://api.sally.co:443/
Location: https://api.sally.co:443/ [following]
Spider mode enabled. Check if remote file exists.
--2024-01-23 19:40:26--  https://api.sally.co/
Connecting to api.sally.co (api.sally.co)|52.72.90.200|:443... connected.
HTTP request sent, awaiting response...
  HTTP/1.1 200 OK
  Date: Wed, 24 Jan 2024 00:40:26 GMT
  Content-Type: text/html; charset=UTF-8
  Content-Length: 1140
  Connection: keep-alive
  X-Powered-By: Express
  Access-Control-Allow-Origin: *
  Access-Control-Allow-Credentials: true
  Accept-Ranges: bytes
  Cache-Control: public, max-age=0
  Last-Modified: Tue, 23 Jan 2024 22:52:32 GMT
  ETag: W/"474-18d38867636"
Length: 1140 (1.1K) [text/html]
Remote file exists and could contain further links,
but recursion is disabled -- not retrieving.
```

>A success response should contain `HTTP/1.1 200 OK`.

### Access the `operations-ui-v2` application using Ingress

Verify the `operations-ui-v2` application endpoint health using `wget`:

```shell
wget -S --spider $(kubectl get -n production ingress operations-ui-v2 -o jsonpath='{.spec.rules[0].host}')
```

The expected output should look like the following:

```text
Spider mode enabled. Check if remote file exists.
--2024-01-24 09:29:57--  http://ops.sally.co/
Resolving ops.sally.co (ops.sally.co)... 107.21.209.87, 3.216.180.32, 44.207.182.168
Connecting to ops.sally.co (ops.sally.co)|107.21.209.87|:80... connected.
HTTP request sent, awaiting response...
  HTTP/1.1 301 Moved Permanently
  Server: awselb/2.0
  Date: Wed, 24 Jan 2024 14:29:57 GMT
  Content-Type: text/html
  Content-Length: 134
  Connection: keep-alive
  Location: https://ops.sally.co:443/
Location: https://ops.sally.co:443/ [following]
Spider mode enabled. Check if remote file exists.
--2024-01-24 09:29:57--  https://ops.sally.co/
Connecting to ops.sally.co (ops.sally.co)|107.21.209.87|:443... connected.
HTTP request sent, awaiting response...
  HTTP/1.1 200 OK
  Date: Wed, 24 Jan 2024 14:29:57 GMT
  Content-Type: text/html; charset=utf-8
  Content-Length: 5039
  Connection: keep-alive
  Cache-Control: private, no-cache, no-store, max-age=0, must-revalidate
  ETag: "8slx55u36v3vz"
  Vary: Accept-Encoding
Length: 5039 (4.9K) [text/html]
Remote file exists and could contain further links,
but recursion is disabled -- not retrieving.
```

>A success response should contain `HTTP/1.1 200 OK`.


### Container Metrics

Check the `api-core-v2` application's CPU and memory metrics:

```shell
kubectl top pods -n production -l app.kubernetes.io/instance=api-core-v2
```

The expected output should look like the following:

```text
NAME                           CPU(cores)   MEMORY(bytes)
api-core-v2-8475d89cbd-fxm82   10m          788Mi
```

Check the `operations-ui-v2` application's CPU and memory metrics:

```shell
kubectl top pods -n production -l app.kubernetes.io/instance=operations-ui-v2
```

The expected output should look like the following:

```text
NAME                                CPU(cores)   MEMORY(bytes)
operations-ui-v2-57f4d68f4b-4l8hc   4m           116Mi
```

Check the CPU and memory metrics for all pods for Addons and Workloads:

```shell
kubectl top pods -A
```

The expected output should look like the following:

```text
NAMESPACE               NAME                                                         CPU(cores)   MEMORY(bytes)
argocd                  argo-cd-argocd-application-controller-0                      67m          281Mi
argocd                  argo-cd-argocd-applicationset-controller-5db688844c-fzqhq    1m           28Mi
argocd                  argo-cd-argocd-dex-server-cd48d7bc-ht7ck                     1m           23Mi
argocd                  argo-cd-argocd-notifications-controller-7d7ccc6b9d-mvln5     1m           21Mi
argocd                  argo-cd-argocd-redis-7f89c69877-vslst                        2m           6Mi
argocd                  argo-cd-argocd-repo-server-644b9b5668-c7r68                  16m          109Mi
argocd                  argo-cd-argocd-repo-server-644b9b5668-m6lsn                  1m           113Mi
argocd                  argo-cd-argocd-server-57cbbd6f94-7m5t5                       4m           45Mi
cert-manager            cert-manager-7d46dc8684-rrgtf                                1m           35Mi
cert-manager            cert-manager-cainjector-7b76c495db-v8zzb                     2m           35Mi
cert-manager            cert-manager-webhook-76d4989968-ptqrk                        1m           15Mi
external-dns            external-dns-76bf7d5c9d-m6hzt                                1m           24Mi
karpenter               karpenter-b8fb695c5-jmdb8                                    7m           76Mi
karpenter               karpenter-b8fb695c5-pjsvx                                    2m           46Mi
kube-prometheus-stack   alertmanager-kube-prometheus-stack-alertmanager-0            1m           31Mi
kube-prometheus-stack   kube-prometheus-stack-grafana-d889bf54b-zn7pb                1m           284Mi
kube-prometheus-stack   kube-prometheus-stack-kube-state-metrics-68d977bb59-ft7xl    2m           17Mi
kube-prometheus-stack   kube-prometheus-stack-operator-66878c5b5d-q6sd7              1m           37Mi
kube-prometheus-stack   kube-prometheus-stack-prometheus-node-exporter-7qr4t         2m           10Mi
kube-prometheus-stack   kube-prometheus-stack-prometheus-node-exporter-jbdmg         2m           14Mi
kube-prometheus-stack   kube-prometheus-stack-prometheus-node-exporter-x4jpz         1m           9Mi
kube-prometheus-stack   prometheus-kube-prometheus-stack-prometheus-0                12m          562Mi
kube-system             aws-load-balancer-controller-78fc98dbd9-9457g                1m           19Mi
kube-system             aws-load-balancer-controller-78fc98dbd9-p26kb                2m           28Mi
kube-system             aws-node-bcpwl                                               3m           57Mi
kube-system             aws-node-jhzz7                                               3m           63Mi
kube-system             aws-node-m9sjb                                               3m           62Mi
kube-system             cluster-autoscaler-aws-cluster-autoscaler-586f6f97c5-tl28d   3m           52Mi
kube-system             coredns-78bdcc48dc-vq8ml                                     2m           22Mi
kube-system             coredns-78bdcc48dc-zhdqg                                     2m           20Mi
kube-system             ebs-csi-controller-5985f98d8-2h64z                           3m           97Mi
kube-system             ebs-csi-controller-5985f98d8-mml8w                           1m           54Mi
kube-system             ebs-csi-node-gvc54                                           1m           26Mi
kube-system             ebs-csi-node-jc66l                                           1m           24Mi
kube-system             ebs-csi-node-xcl44                                           1m           38Mi
kube-system             kube-proxy-2pqbg                                             1m           16Mi
kube-system             kube-proxy-dl2w6                                             1m           23Mi
kube-system             kube-proxy-pngqf                                             1m           17Mi
kube-system             metrics-server-5b76987ff-fd622                               4m           22Mi
production              api-core-v2-8475d89cbd-fxm82                                 8m           789Mi
production              camunda-bpm-platform-5dbb896b45-kxh7t                        1m           302Mi
production              keycloak-0                                                   3m           478Mi
production              keycloak-1                                                   3m           477Mi
production              minio-service-5bb59b4694-79rvb                               1m           45Mi
production              minio-service-5bb59b4694-t9xgv                               2m           46Mi
production              operations-ui-v2-57f4d68f4b-4l8hc                            3m           116Mi
production              rabbitmq-0                                                   3m           110Mi
production              redis-master-0                                               20m          5Mi
production              redis-replicas-0                                             22m          4Mi
production              redis-replicas-1                                             18m          4Mi
vault                   vault-0                                                      28m          75Mi
vault                   vault-1                                                      36m          216Mi
vault                   vault-secrets-operator-controller-manager-ff7ff47dd-khgfn    2m           40Mi
```

## Give permission to interact with Kubernetes

In order to give permission to run `kubectl` commands in Sally cluster we'll need to create the proper `Role` and `RoleBinding`, for this you should run ./k8s/rbac/apply-team-role-rolebinding.sh. And we will also need to patch `aws-auth`, for this you can run ./k8s/aws-auth-patch.sh.

## Collect metrics from `api-core`
Run those commands to provide access to Prometheus to collect metrics from Sally's api.
```bash
kubectl apply -f k8s/metrics/prom-api-core-exporter.yaml
kubectl apply -f k8s/metrics/prom-operator-rbac.yaml
```

## Destroy the EKS Cluster

To tear down all the resources and the EKS cluster, run the following command:

```shell
./destroy.sh
```

> ⚠️ **Important:** Keycloak, Vault, RabbitMQ and Sally deal with data persistency, therefore `PostgreSQL`, vault's and rabbitmq's `pvc` and `DocumentDB` will not be deteled automatically. If you really want to delete it, you'll need to do it manually for security reasons.

> ⛔️ **Danger:** It will destroy all AWS infrastructure resources for Sally Production! Are you sure you still want to do it? If you are still considering it, first flip `deletion_protection` and `skip_final_snapshot` to `false` and `true` respectively for both PostgreSQL and DocumentDB databases.

## Appendix

## Fork GitOps Repositories

To modify the `values.yaml` file for addons or the workload manifest files (.ie yaml), you'll need to fork two repositories: [aws-samples/eks-blueprints-add-ons](https://github.com/aws-samples/eks-blueprints-add-ons) for addons and [github.com/aws-ia/terraform-aws-eks-blueprints](https://github.com/aws-ia/terraform-aws-eks-blueprints) for workloads located in this pattern directory.

After forking, update the following environment variables to point to your forks, replacing the default values.

```shell
export TF_VAR_gitops_addons_org=https://github.com/aws-samples
export TF_VAR_gitops_addons_repo=eks-blueprints-add-ons
export TF_VAR_gitops_addons_revision=main

export TF_VAR_gitops_workload_org=https://github.com/aws-ia
export TF_VAR_gitops_workload_repo=terraform-aws-eks-blueprints
export TF_VAR_gitops_workload_revision=main
```
