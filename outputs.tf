output "configure_kubectl" {
  description = "Configure kubectl: make sure you're logged in with the correct AWS profile and run the following command to update your kubeconfig"
  value       = <<-EOT
    export KUBECONFIG="/tmp/${module.eks.cluster_name}"
    aws eks --region ${local.region} update-kubeconfig --name ${module.eks.cluster_name}
  EOT
}

output "configure_argocd" {
  description = "Terminal Setup"
  value       = <<-EOT
    export KUBECONFIG="/tmp/${module.eks.cluster_name}"
    aws eks --region ${local.region} update-kubeconfig --name ${module.eks.cluster_name}
    export ARGOCD_OPTS="--port-forward --port-forward-namespace argocd --grpc-web"
    kubectl config set-context --current --namespace argocd
    argocd login --port-forward --username admin --password $(argocd admin initial-password | head -1)
    echo "ArgoCD Username: admin"
    echo "ArgoCD Password: $(kubectl get secrets argocd-initial-admin-secret -n argocd --template="{{index .data.password | base64decode}}")"
    echo Port Forward: http://localhost:8080
    kubectl port-forward -n argocd svc/argo-cd-argocd-server 8080:80
    EOT
}

output "access_argocd" {
  description = "ArgoCD Access"
  value       = <<-EOT
    export KUBECONFIG="/tmp/${module.eks.cluster_name}"
    aws eks --region ${local.region} update-kubeconfig --name ${module.eks.cluster_name}
    echo "ArgoCD Username: admin"
    echo "ArgoCD Password: $(kubectl get secrets argocd-initial-admin-secret -n argocd --template="{{index .data.password | base64decode}}")"
    echo "ArgoCD URL: https://$(kubectl get ing -n argocd argo-cd-argocd-server -o jsonpath='{.spec.rules[0].host}')"
    EOT
}

output "access_prometheus_grafana" {
  description = "Prometheus Grafana Access"
  value       = <<-EOT
    export KUBECONFIG="/tmp/${module.eks.cluster_name}"
    aws eks --region ${local.region} update-kubeconfig --name ${module.eks.cluster_name}
    echo "Prometheus Grafana Username: $(kubectl get secret kube-prometheus-stack-grafana -n kube-prometheus-stack -o jsonpath="{.data.admin-user}" | base64 --decode)"
    echo "Prometheus Grafana Password: $(kubectl get secret kube-prometheus-stack-grafana -n kube-prometheus-stack -o jsonpath="{.data.admin-password}" | base64 --decode)"
    EOT
}

output "access_minio" {
  value = <<-EOT
    export KUBECONFIG="/tmp/${module.eks.cluster_name}"
    aws eks --region ${local.region} update-kubeconfig --name ${module.eks.cluster_name}
    echo "MinIO Username: $(kubectl get secret minio-service -n production -o jsonpath="{.data.root-user}" | base64 --decode)"
    echo "MinIO Password: $(kubectl get secret minio-service -n production -o jsonpath="{.data.root-password}" | base64 --decode)"
    echo "MinIO URL: https://$(kubectl get ing -n production minio-service -o jsonpath='{.spec.rules[0].host}')"
    EOT
}

output "access_postgresql" {
  sensitive = true
  value     = <<-EOT
    export KUBECONFIG="/tmp/${module.eks.cluster_name}"
    aws eks --region ${local.region} update-kubeconfig --name ${module.eks.cluster_name}
    echo "PostgreSQL Username: ${aws_db_instance.postgres_db.username}"
    echo "PostgreSQL Password: ${aws_db_instance.postgres_db.password}"
    echo "PostgreSQL Hostname: ${aws_db_instance.postgres_db.address}"
    echo "PostgreSQL DB Name: ${aws_db_instance.postgres_db.db_name}"
    EOT
}

output "access_keycloak" {
  sensitive = true
  value     = <<-EOT
    export KUBECONFIG="/tmp/${module.eks.cluster_name}"
    aws eks --region ${local.region} update-kubeconfig --name ${module.eks.cluster_name}
    echo "Keycloak Username: admin"
    echo "Keycloak Admin Password: $(kubectl get secret keycloak-pass -n production -o jsonpath="{.data.password}" | base64 --decode)"
    echo "Keycloak URL: https://$(kubectl get ing -n production keycloak -o jsonpath='{.spec.rules[1].host}')"
    EOT
}

# output "access_rabbitmq" {
#   sensitive   = true
#   value       = <<-EOT
#     export KUBECONFIG="/tmp/${module.eks.cluster_name}"
#     aws eks --region ${local.region} update-kubeconfig --name ${module.eks.cluster_name}
#     echo "RabbitMQ Username: user"
#     echo "RabbitMQ Password: $(kubectl get secret rabbitmq -n production -o jsonpath="{.data.rabbitmq-password}" | base64 --decode)"
#     echo "RabbitMQ URL: https://$(kubectl get ing -n production rabbitmq -o jsonpath='{.spec.rules[0].host}')"
#     EOT
# }

# output "access_bullboard" {
#   sensitive = true
#   value     = <<-EOT
#   export KUBECONFIG="/tmp/${module.eks.cluster_name}"
#   aws eks --region ${local.region} update-kubeconfig --name ${module.eks.cluster_name}
#   echo "Bull Board Username: $(kubectl get secret anet-webhook-secret -n production -o jsonpath='{.data.API_LOGIN_ID}' | base64 --decode)"
#   echo "Bull Board Password: $(kubectl get secret anet-webhook-secret -n production -o jsonpath='{.data.TRANSACTION_KEY}' | base64 --decode)"
#   echo "Bullboard URL: https://$(kubectl get ing -n production anet-webhook-ingress -o jsonpath='{.spec.rules[0].host}')"
#   EOT
# }

output "access_anet_webhook_monitor" {
  sensitive = true
  value     = <<-EOT
  export KUBECONFIG="/tmp/${module.eks.cluster_name}"
  aws eks --region ${local.region} update-kubeconfig --name ${module.eks.cluster_name}
  echo "Bull Board Username: $(kubectl get secret bullboard-secret -n production -o jsonpath='{.data.BULLBOARD_USER}' | base64 --decode)"
  echo "Bull Board Password: $(kubectl get secret bullboard-secret -n production -o jsonpath='{.data.BULLBOARD_PASSWORD}' | base64 --decode)"
  echo "Bullboard URL: https://$(kubectl get ing -n production bullboard-ingress -o jsonpath='{.spec.rules[0].host}')"
  EOT
}

output "access_documentdb" {
  sensitive = true
  value     = <<-EOT
    export KUBECONFIG="/tmp/${module.eks.cluster_name}"
    aws eks --region ${local.region} update-kubeconfig --name ${module.eks.cluster_name}
    echo "DocumentDB Username: ${aws_docdb_cluster.sally_prod.master_username}"
    echo "DocumentDB Password: ${aws_docdb_cluster.sally_prod.master_password}"
    echo "DocumentDB Hostname: ${aws_docdb_cluster.sally_prod.endpoint}"
    EOT
}

output "onboarding_access_key_id" {
  sensitive = true
  value     = <<-EOT
    echo "Onboarding access key: ${aws_iam_access_key.onboarding.id}"
  EOT
}

output "onboarding_secret_access_key" {
  sensitive = true
  value     = <<-EOT
    echo "Onboarding secret key: ${aws_iam_access_key.onboarding.secret}"
  EOT
}

output "cloudfront_acm_certificate_arn" {
  description = "ARN of the ACM certificate used for CloudFront distribution"
  value       = var.acm_certificate_arn
}
