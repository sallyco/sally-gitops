resource "aws_iam_user" "onboarding" {
  name = "onboarding"
  tags = {
    Environment = "prod"
    Name        = "sally"
  }
}

resource "aws_iam_access_key" "onboarding" {
  user = aws_iam_user.onboarding.name
}

resource "aws_iam_policy" "onboarding_policy" {
  name        = "onboardingPolicy"
  description = "Policy for Onboarding User to manage Route53, CloudFront, and SES"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = [
          "route53:ListHostedZones",
          "route53:GetHostedZone",
          "route53:ChangeResourceRecordSets",
          "cloudfront:GetDistribution",
          "cloudfront:UpdateDistribution",
          "cloudfront:ListDistributions",
          "ses:VerifyEmailAddress",
          "ses:GetIdentityVerificationAttributes"
        ]
        Resource = "*"
      },
    ]
  })
}

resource "aws_iam_user_policy_attachment" "onboarding_policy_attachment" {
  user       = aws_iam_user.onboarding.name
  policy_arn = aws_iam_policy.onboarding_policy.arn
}
