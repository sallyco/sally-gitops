# Create AWS Backup vault
resource "aws_backup_vault" "vault_backup" {
  name = "vault-backup-${var.name}"
  tags = {
    Environment = "production"
    Service     = "vault"
  }
}

# Create backup plan
resource "aws_backup_plan" "vault" {
  name = "vault-backup-plan"

  rule {
    rule_name         = "vault_daily_backup"
    target_vault_name = aws_backup_vault.vault_backup.name
    schedule          = "cron(0 5 ? * * *)" # Daily at 5 AM UTC

    lifecycle {
      delete_after = 30 # Keep backups for 30 days
    }

    copy_action {
      destination_vault_arn = aws_backup_vault.vault_backup.arn
    }
  }

  advanced_backup_setting {
    backup_options = {
      WindowsVSS = "enabled"
    }
    resource_type = "EC2"
  }
}

# Create IAM role for AWS Backup
resource "aws_iam_role" "vault_backup" {
  name = "vault-backup-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "backup.amazonaws.com"
        }
      }
    ]
  })
}

# Attach AWS Backup service role policy
resource "aws_iam_role_policy_attachment" "vault_backup" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForBackup"
  role       = aws_iam_role.vault_backup.name
}

# Create backup selection
resource "aws_backup_selection" "vault" {
  name         = "vault-backup-selection"
  iam_role_arn = aws_iam_role.vault_backup.arn
  plan_id      = aws_backup_plan.vault.id

  selection_tag {
    type  = "STRINGEQUALS"
    key   = "kubernetes.io/cluster/sally"
    value = "owned"
  }
}