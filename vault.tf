resource "aws_iam_user" "vault" {
  name = "sally-vault"
  path = "/"
}

resource "aws_iam_user_policy" "vault" {
  name = "sally-vault-kms"
  user = aws_iam_user.vault.name
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "kms:Encrypt",
          "kms:Decrypt",
          "kms:DescribeKey",
          "kms:ReEncrypt*",
          "kms:GenerateDataKey*",
          "kms:CreateGrant"
        ],
        "Resource" : "${aws_kms_key.vault.arn}"
      }
    ]
  })
}

resource "aws_iam_access_key" "vault" {
  user = aws_iam_user.vault.name
}

resource "kubernetes_secret" "vault_kms" {
  metadata {
    name      = "sally-vault-kms"
    namespace = kubernetes_namespace.vault.metadata.0.name
  }
  data = {
    AWS_ACCESS_KEY_ID     = aws_iam_access_key.vault.id
    AWS_SECRET_ACCESS_KEY = aws_iam_access_key.vault.secret
  }
}

# To autounseal vault due to high availability
resource "aws_kms_key" "vault" {
  description = "Sally production Vault Unseal Key"
  tags = {
    Name = "Sally Vault KMS Unseal Key"
  }
}

resource "aws_kms_alias" "vault_alias" {
  name          = "alias/vault/${var.name}"
  target_key_id = aws_kms_key.vault.key_id
}

# Installing Vault Server and UI only
resource "helm_release" "vault" {
  repository = "https://helm.releases.hashicorp.com"
  name       = "vault"
  chart      = "vault"
  namespace  = kubernetes_namespace.vault.metadata.0.name
  values = [
    templatefile(
      "${path.root}/helm_values/vault.yaml",
      {
        iam-secret-name = kubernetes_secret.vault_kms.metadata.0.name,
        kms-region      = var.region,
        kms-key-id      = aws_kms_key.vault.id
      }
    )
  ]
}

# This syncronizes vault secrets with kubernetes secrets
resource "helm_release" "vault_secrets_operator" {
  name       = "vault-secrets-operator"
  repository = "https://helm.releases.hashicorp.com"
  chart      = "vault-secrets-operator"
  namespace  = kubernetes_namespace.vault.metadata.0.name
  version    = "0.4.2"
  values = [
    templatefile(
      "${path.root}/helm_values/vault-secrets-operator.yaml",
      {
        prod-namespace-name  = kubernetes_namespace.production.metadata.0.name,
        vault-namespace-name = kubernetes_namespace.vault.metadata.0.name
      }
    )
  ]
}
