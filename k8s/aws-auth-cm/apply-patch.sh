#!/bin/bash

# Backing up the Current aws-auth ConfigMap
kubectl get configmap/aws-auth -n kube-system -o yaml > aws-auth-backup.yaml

# Applying the Patch
kubectl patch configmap/aws-auth -n kube-system --patch "$(cat aws-auth-patch.yaml)"

#Verifying the changes
kubectl get configmap/aws-auth -n kube-system -o yaml
