#!/bin/bash

# Applying the Role
kubectl apply -f sally-team-role.yaml

# Applying the RoleBinding
kubectl apply -f sally-team-rolebinding.yaml