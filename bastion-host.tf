data "http" "my_ip" {
  url = "https://api.ipify.org"
}

# Create sally-general-purpose-key-pair.pub file using ssh-keygen and copy it to
# Don't forget to add the public key to your GitHub account
# Don't forget to add the private key to your ssh-agent and copy it to your ~/.ssh directory. 
# You'll also need to chmod 400 ~/.ssh/sally-general-purpose-key-pair 
resource "aws_key_pair" "sally_key_pair" {
  key_name   = var.bastion_host_key_pair_name
  public_key = file("sally-general-purpose-key-pair.pub")

  tags = {
    Name = "Sally Key Pair"
  }
}

output "sally_key_pair_private_key" {
  value     = aws_key_pair.sally_key_pair.key_name
  sensitive = true
}

data "aws_vpc" "selected_vpc" {
  id = module.vpc.vpc_id
}

data "aws_subnet" "selected_subnet" {
  id = module.vpc.public_subnets[0]
}

resource "aws_security_group" "sally_sg" {
  name        = "sally-bh-sg"
  description = "Sally Bastion Host Security Group"
  vpc_id      = data.aws_vpc.selected_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = concat(["${chomp(data.http.my_ip.body)}/32"],var.bastion_host_cidr_blocks)
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Sally Bastion Host Security Group"
  }
}

resource "aws_instance" "sally_bastion_host" {
  ami           = var.bastion_host_ami
  instance_type = var.bastion_host_instance_type
  key_name      = aws_key_pair.sally_key_pair.key_name
  subnet_id     = data.aws_subnet.selected_subnet.id
  vpc_security_group_ids = [aws_security_group.sally_sg.id]

  associate_public_ip_address = true

  root_block_device {
    volume_type = var.bastion_host_volume_type
    volume_size = var.bastion_host_volume_size
  }

  tags = {
    Name = "Sally Bastion Host"
  }
}

resource "aws_s3_bucket" "sally_engineers_bucket" {
  bucket = "sally-engineers"

  tags = {
    Name = "Sally Engineers Bucket"
  }
}
