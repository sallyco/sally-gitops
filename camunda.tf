# resource "random_password" "camunda_admin_password" {
#   length = 10
# }
# resource "kubernetes_secret" "camunda_admin_creds" {
#   metadata {
#     name = "camunda-admin-creds"
#     namespace = kubernetes_namespace.production.metadata[0].name
#   }
#   type = "kubernetes.io/basic-auth"
#   data = {
#     username = "admin"
#     password = random_password.camunda_admin_password.result
#   }
# }
# resource "kubernetes_secret" "camunda_db_creds" {
#   metadata {
#     name = "camunda-db-creds"
#     namespace = kubernetes_namespace.production.metadata[0].name
#   }
#   data = {
#     DB_USERNAME = var.keycloak_username
#     DB_PASSWORD = aws_db_instance.postgres_db.password
#   }
# }
#
# resource "helm_release" "camunda" {
#   repository = "https://helm.cch.camunda.cloud"
#   chart = "camunda-bpm-platform"
#   name  = "camunda-bpm-platform"
#   namespace = kubernetes_namespace.production.metadata[0].name
#   version = "7.6.7"
#   values = [templatefile("${path.root}/helm_values/camunda.yaml", {
#     db-url=aws_db_instance.postgres_db.endpoint,
#     db-hostname=aws_db_instance.postgres_db.address,
#     keycloak-db-name=aws_db_instance.postgres_db.db_name,
#     camunda-db-name=var.camunda_db_name,
#     db-credentials-secret-name=kubernetes_secret.camunda_db_creds.metadata.0.name,
#     admin-credentials-secret-name=kubernetes_secret.camunda_admin_creds.metadata.0.name,
#     admin-password = random_password.camunda_admin_password.result
#   })]
# }
# resource "kubernetes_config_map" "camunda-config" {
#   metadata {
#     name = "camunda-config"
#     namespace = kubernetes_namespace.production.metadata.0.name
#   }
#   data = {
#     "default.yml" = <<EOF
# camunda.bpm:
#   admin-user:
#     id: admin
#   run:
#     cors:
#       enabled: true
#       allowed-origins: "*"
#   auto-deployment-enabled: false
#
# spring.web.resources:
#   static-locations: NULL
# EOF
#   }
# }
