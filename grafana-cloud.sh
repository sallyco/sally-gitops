#!/bin/bash
helm repo add grafana https://grafana.github.io/helm-charts &&
  helm repo update &&
  helm upgrade --install --atomic --timeout 300s grafana-k8s-monitoring grafana/k8s-monitoring \
    --namespace "monitoring" --create-namespace --values - <<EOF
cluster:
  name: sally
externalServices:
  prometheus:
    host: https://prometheus-prod-13-prod-us-east-0.grafana.net
    basicAuth:
      username: "1470357"
      password: glc_eyJvIjoiMTA3NDQ3NyIsIm4iOiJzdGFjay04Nzg4MTAtaW50ZWdyYXRpb24tc2FsbHktdG9rZW4tc2FsbHktdG9rZW4iLCJrIjoiM3lFV0ZwMzlpM3Z6T3g0ODk3SDkyYnpSIiwibSI6eyJyIjoicHJvZC11cy1lYXN0LTAifX0=
  loki:
    host: https://logs-prod-006.grafana.net
    basicAuth:
      username: "836591"
      password: glc_eyJvIjoiMTA3NDQ3NyIsIm4iOiJzdGFjay04Nzg4MTAtaW50ZWdyYXRpb24tc2FsbHktdG9rZW4tc2FsbHktdG9rZW4iLCJrIjoiM3lFV0ZwMzlpM3Z6T3g0ODk3SDkyYnpSIiwibSI6eyJyIjoicHJvZC11cy1lYXN0LTAifX0=
  tempo:
    host: https://tempo-prod-04-prod-us-east-0.grafana.net:443
    basicAuth:
      username: "830907"
      password: glc_eyJvIjoiMTA3NDQ3NyIsIm4iOiJzdGFjay04Nzg4MTAtaW50ZWdyYXRpb24tc2FsbHktdG9rZW4tc2FsbHktdG9rZW4iLCJrIjoiM3lFV0ZwMzlpM3Z6T3g0ODk3SDkyYnpSIiwibSI6eyJyIjoicHJvZC11cy1lYXN0LTAifX0=
metrics:
  enabled: true
  cost:
    enabled: true
  node-exporter:
    enabled: false
logs:
  enabled: true
  pod_logs:
    enabled: true
  cluster_events:
    enabled: true
traces:
  enabled: true
receivers:
  grpc:
    enabled: true
  http:
    enabled: true
  zipkin:
    enabled: true
opencost:
  enabled: true
  opencost:
    exporter:
      defaultClusterId: sally
    prometheus:
      external:
        url: https://prometheus-prod-13-prod-us-east-0.grafana.net/api/prom
kube-state-metrics:
  enabled: true
prometheus-node-exporter:
  enabled: false
prometheus-operator-crds:
  enabled: true
grafana-agent: {}
grafana-agent-logs: {}
EOF