resource "aws_security_group" "postgres_sg" {
  name        = "sally-postgres-sg"
  description = "Security group for PostgreSQL DB"
  vpc_id      = module.vpc.vpc_id

  tags = {
    Name = "Sally Postgres Security Group"
  }
}

resource "aws_security_group_rule" "postgres_inbound" {
  type              = "ingress"
  from_port         = 5432
  to_port           = 5432
  protocol          = "tcp"
  cidr_blocks       = concat(module.vpc.private_subnets_cidr_blocks, module.vpc.public_subnets_cidr_blocks)
  security_group_id = aws_security_group.postgres_sg.id
}


resource "aws_db_subnet_group" "keycloak_prod_sally" {
  name       = "keycloakprodsally"
  subnet_ids = module.vpc.private_subnets

  tags = {
    Name = "Sally Keycloak Production Subnet Group"
  }
}

resource "random_password" "keycloak_db_password" {
  length           = 25
  special          = true
  override_special = "!#$%&()*+,-.:;<=>?[]^_{|}~"
}

resource "aws_db_parameter_group" "custom_postgres16" {
  name        = "postgres16-custom"
  family      = "postgres16"
  description = "Custom postgres16 parameter group"

  parameter {
    name  = "rds.force_ssl"
    value = "0"
  }
}

resource "aws_db_instance" "postgres_db" {
  identifier                      = var.keycloak_db_name
  db_subnet_group_name            = aws_db_subnet_group.keycloak_prod_sally.name
  ca_cert_identifier              = "rds-ca-rsa2048-g1"
  deletion_protection             = true
  multi_az                        = true
  backup_retention_period         = 7
  allocated_storage               = 20
  storage_type                    = "gp3"
  engine                          = "postgres"
  engine_version                  = "16.3"
  instance_class                  = "db.t3.small"
  db_name                         = var.keycloak_db_name
  username                        = var.keycloak_username
  password                        = random_password.keycloak_db_password.result
  skip_final_snapshot             = false
  vpc_security_group_ids          = [aws_security_group.postgres_sg.id]
  storage_encrypted               = true
  enabled_cloudwatch_logs_exports = ["postgresql", "upgrade"]
  publicly_accessible             = false
  parameter_group_name            = aws_db_parameter_group.custom_postgres16.name
}

resource "random_password" "keycloak_admin_password" {
  length = 25
}

resource "kubernetes_secret" "keycloak_admin_pass" {
  metadata {
    name      = "keycloak-pass"
    namespace = kubernetes_namespace.production.metadata.0.name
  }
  type = "Opaque"
  data = {
    "password" = random_password.keycloak_admin_password.result
  }
}

resource "helm_release" "keycloak" {
  chart        = "keycloakx"
  version      = "1.3.2"
  repository   = "https://codecentric.github.io/helm-charts"
  name         = "keycloak"
  namespace    = kubernetes_namespace.production.metadata.0.name
  force_update = true

  values = [
    templatefile("${path.root}/helm_values/keycloak.yaml", {
      hostname    = "auth.sally.com",
      kc-admin    = random_password.keycloak_admin_password.result,
      db-host     = aws_db_instance.postgres_db.address,
      db-user     = aws_db_instance.postgres_db.username,
      db-password = aws_db_instance.postgres_db.password,
      db-name     = aws_db_instance.postgres_db.db_name
    })
  ]
  depends_on = [kubernetes_config_map.keycloak_realm]
}

resource "kubernetes_config_map" "keycloak_realm" {
  metadata {
    name      = "keycloak-realm-config"
    namespace = kubernetes_namespace.production.metadata.0.name
  }

  data = {
    "realms.json" = file("${path.module}/config/keycloak/realms.json")
  }
}

# Provides DocumentDB metrics for Prometheus
resource "helm_release" "postgres_exporter" {
  name       = "postgres-exporter"
  repository = "https://prometheus-community.github.io/helm-charts"
  version    = "5.3.0"
  chart      = "prometheus-postgres-exporter"
  namespace  = kubernetes_namespace.monitoring.metadata[0].name

  values = [
    templatefile("${path.module}/helm_values/prom-postgres-exporter.yaml", {
      postgres_hostname = aws_db_instance.postgres_db.address,
      postgres_port     = aws_db_instance.postgres_db.port,
      postgres_user     = aws_db_instance.postgres_db.username,
      postgres_password = aws_db_instance.postgres_db.password,
      postgres_dbname   = aws_db_instance.postgres_db.db_name
    })
  ]
  depends_on = [aws_docdb_cluster.sally_prod]
}
