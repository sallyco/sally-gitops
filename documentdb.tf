###### DocumentDB ######

resource "aws_security_group" "documentdb_sg" {
  name        = "sally-docdb-sg"
  description = "Security group for DocumentDB"
  vpc_id      = module.vpc.vpc_id

  tags = {
    Name = "Sally DocumentDB Security Group"
  }
}

resource "aws_security_group_rule" "documentdb_inbound" {
  type        = "ingress"
  from_port   = 27017
  to_port     = 27017
  protocol    = "tcp"
  cidr_blocks = concat(module.vpc.private_subnets_cidr_blocks,module.vpc.public_subnets_cidr_blocks)
  security_group_id = aws_security_group.documentdb_sg.id
}


resource "aws_db_subnet_group" "documentdb_sally" {
  name       = "documentdbsally"
  subnet_ids = module.vpc.private_subnets

  tags = {
    Name = "Sally DocumentDB Production Subnet Group"
  }
}


resource "aws_kms_key" "sally_prod_key" {
  description = "Sally key to encrypt and protect prod data"
  tags = {
    Name = "KMS key for Sally DocumentDB production"
  }
}

resource "aws_iam_policy" "sally_prod_kms_policy" {
  name        = "sally-prod-kms-policy"
  description = "Policy to allow Sally DocumentDB to access KMS key"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = [
          "kms:Encrypt",
          "kms:Decrypt",
          "kms:ReEncrypt*",
          "kms:GenerateDataKey*",
          "kms:DescribeKey"
        ],
        Effect   = "Allow",
        Resource = aws_kms_key.sally_prod_key.arn
      }
    ]
  })
  tags = {
    Name = "IAM policy for Sally DocumentDB production to access KMS key"
  }
}

resource "aws_iam_policy" "sally_prod_cloudwatch_policy" {
  name        = "sally-prod-cloudwatch-policy"
  description = "Policy to allow Sally DocumentDB to write logs to CloudWatch"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = [
          "logs:CreateLogStream",
          "logs:PutLogEvents",
          "logs:CreateLogGroup"
        ],
        Resource = "arn:aws:logs:*:*:*"
      }
    ]
  })

  tags = {
    Name = "IAM policy for Sally DocumentDB production to access CloudWatch Logs"
  }
}

resource "aws_iam_role_policy_attachment" "sally_prod_cloudwatch_policy_attachment" {
  role       = aws_iam_role.sally_prod_role.name
  policy_arn = aws_iam_policy.sally_prod_cloudwatch_policy.arn
}

resource "aws_iam_role" "sally_prod_role" {
  name = "sally-prod-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "rds.amazonaws.com"
        }
      }
    ]
  })
  tags = {
    Name = "IAM Role for Sally DocumentDB production"
  }
}

resource "aws_iam_role_policy_attachment" "sally_prod_kms_policy_attachment" {
  role       = aws_iam_role.sally_prod_role.name
  policy_arn = aws_iam_policy.sally_prod_kms_policy.arn
}


resource "random_password" "sally_prod_master_password" {
  length = 25
  special          = true
  override_special = "!#$%&()*+,-.:;<=>?[]^_{|}~"
}

resource "aws_docdb_cluster" "sally_prod" {
  cluster_identifier              = "${var.name}prod"
  db_subnet_group_name            = aws_db_subnet_group.documentdb_sally.name
  vpc_security_group_ids          = [aws_security_group.documentdb_sg.id]
  deletion_protection             = true
  enabled_cloudwatch_logs_exports = ["audit", "profiler"]
  engine                          = "docdb"
  engine_version                  = "5.0.0"
  master_username                 = var.name # sally
  master_password                 = random_password.sally_prod_master_password.result
  kms_key_id                      = aws_kms_key.sally_prod_key.arn
  storage_encrypted               = true
  backup_retention_period         = 5
  skip_final_snapshot             = false
}

resource "aws_docdb_cluster_instance" "sally_prod_instances" {
  count              = 2
  identifier         = "${var.name}prod${count.index}"
  cluster_identifier = aws_docdb_cluster.sally_prod.id
  instance_class     = "db.r6g.large"
}

# Provides DocumentDB metrics for Prometheus
resource "helm_release" "mongodb_exporter" {
  name       = "mongodb-exporter"
  repository = "https://prometheus-community.github.io/helm-charts"
  version    = "3.5.0"
  chart      = "prometheus-mongodb-exporter"
  namespace  = kubernetes_namespace.monitoring.metadata[0].name

  values = [
    templatefile("${path.module}/helm_values/prom-mongodb-exporter.yaml", {
      mongodb_hostname = aws_docdb_cluster.sally_prod.endpoint
      mongodb_port     = aws_docdb_cluster.sally_prod.port
      mongodb_user     = aws_docdb_cluster.sally_prod.master_username
      mongodb_password = aws_docdb_cluster.sally_prod.master_password
      mongodb_db = aws_docdb_cluster.sally_prod.master_password
    })
  ]
  depends_on = [aws_docdb_cluster.sally_prod]
}
